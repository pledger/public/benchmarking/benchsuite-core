#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import configparser
import glob
import json
import os
import re

import logging
from typing import ClassVar, Optional, List

import yaml
from appdirs import user_data_dir, user_config_dir
from pydantic import BaseModel, Field

from benchsuite.core.model.exception import ControllerConfigurationException

logger = logging.getLogger(__name__)


class ServiceProviderConfiguration():
    """
    Represents the configuration file of a cloud provider
    """

    def __init__(self, config_file):
        self.name = os.path.splitext(os.path.basename(config_file))[0]

        # TODO: use here the functions in provider.py to load the providers from the configuration
        try:
            with open(config_file) as f:
                config = configparser.ConfigParser()
                config.read_dict(json.load(f))
        except ValueError as ex:
            logger.warning('Got an exception trying to decode configuration file as json: ' + str(ex))
            try:
                config = configparser.ConfigParser()
                config.read(config_file)
            except Exception as ex:
                raise ControllerConfigurationException('Invalid configuration provided: {0}'.format(str(ex)))

        # TODO: libcloud_extra_params should not go here because it is something dependant from the implemetnation of
        sections = [s for s in list(config.keys()) if s != 'DEFAULT' and s != 'provider' and s != 'libcloud_extra_params']

        self.service_types = sections

    def __str__(self) -> str:
        return '{0}: {1}'.format(self.name, self.service_types)


class BenchmarkToolConfiguration():
    """
    Represents the configuration of a benchmark tool
    """

    def __init__(self, config_file):
        logger.info(f"Loading benchmark definition from {config_file}")
        self.id = os.path.basename(config_file)[:-5]
        self.config_file = config_file

        try:
            logger.info(f"Trying to load {config_file} as YAML benchmark")
            with open(config_file, 'r') as stream:
                config = yaml.safe_load(stream)
                self.tool_name = config['tool']
                self.workloads = []
                for w, c in config['workloads'].items():
                    self.workloads.append({
                        'id': w,
                        'workload_name': c.get('name', None),
                        'workload_description': c.get('description', None)
                    })
        except Exception as ex:
            logger.info("Loading as YAML failed. Fallback to ConfigParser (Exception: %s)", str(ex))

            config = configparser.ConfigParser()
            config.read(config_file)
            self.tool_name = config['DEFAULT']['tool_name']

            sections = [s for s in list(config.keys()) if s != 'DEFAULT']

            self.workloads = []

            for w in sections:
                self.workloads.append({
                    'id': w,
                    'workload_name': config[w]['workload_name'] if 'workload_name' in config[w] else None,
                    'workload_description': config[w]['workload_description'] if 'workload_description' in config[w] else None
                })

    def find_workloads(self, regex):
        return [w['id'] for w in self.workloads if re.match(regex, w['id'])]

    def __str__(self) -> str:
        return '{0}: {1}'.format(self.tool_name, self.workloads)


class ControllerConfiguration():
    '''
    Model the Benchmarking Suite configuration.
    '''

    CLOUD_PROVIDERS_DIR = 'providers'
    BENCHMARKS_DIR = 'workloads'
    STORAGE_CONFIG_FILE = 'storage.conf'
    STORAGE_JSON_CONFIG_FILE = 'storage.json'

    def __init__(self, alternative_config_dir=None):

        self.default_config_dir = os.path.join(user_config_dir(), 'benchmarking-suite')

        self.alternative_config_dir = alternative_config_dir

        logger.debug('Using default configuration directory: %s', self.default_config_dir)
        logger.debug('Using alternative configuration directory: %s', self.alternative_config_dir)

    def get_default_data_dir(self):
        d = user_data_dir('benchmarking-suite', None)
        if not os.path.exists(d):
            os.makedirs(d)
        return d

    def list_available_providers(self):
        """
        Lists all the ServiceProvider configuration files found in the configuration folders
        :return: 
        """
        providers = []

        if self.alternative_config_dir:
            for f in os.listdir(os.path.join(self.alternative_config_dir, self.CLOUD_PROVIDERS_DIR)):
                if os.path.splitext(f)[1] in ['.conf', '.json']:
                    try:
                        n = os.path.join(self.alternative_config_dir, self.CLOUD_PROVIDERS_DIR, f)
                        providers.append(ServiceProviderConfiguration(n))
                    except ControllerConfigurationException:
                        pass

        default_dir = os.path.join(self.default_config_dir, self.CLOUD_PROVIDERS_DIR)
        if os.path.exists(default_dir):
            for f in os.listdir(os.path.join(self.default_config_dir, self.CLOUD_PROVIDERS_DIR)):
                if os.path.splitext(f)[1] in ['.conf', '.json']:
                    try:
                        n = os.path.join(self.default_config_dir, self.CLOUD_PROVIDERS_DIR, f)
                        providers.append(ServiceProviderConfiguration(n))
                    except ControllerConfigurationException:
                        pass

        return providers

    def __load_and_append_tool(self, file, list):
        try:
            list.append(BenchmarkToolConfiguration(file))
        except Exception as ex:
            logger.error(f'Error loading benchmarking definition from {file}. Ignoring it and continuing')

    def list_available_tools(self):
        """
        Lists all the Benchmarks configuration files found in the configuration folders
        :return: 
        """
        benchmarks = []

        if self.alternative_config_dir:
            for n in glob.glob(os.path.join(self.alternative_config_dir, self.BENCHMARKS_DIR, '*.conf')):
                self.__load_and_append_tool(n, benchmarks)

        for n in glob.glob(os.path.join(self.default_config_dir, self.BENCHMARKS_DIR, '*.conf')):
            self.__load_and_append_tool(n, benchmarks)

        if self.alternative_config_dir:
            for n in glob.glob(os.path.join(self.alternative_config_dir, '*.conf')):
                self.__load_and_append_tool(n, benchmarks)

        for n in glob.glob(os.path.join(self.default_config_dir, '*.conf')):
            self.__load_and_append_tool(n, benchmarks)

        return benchmarks

    def get_provider_by_name(self, name: str) -> ServiceProviderConfiguration:
        return ServiceProviderConfiguration(self.get_provider_config_file(name))

    def get_benchmark_by_name(self, name):

        # first look within <config_dir>/benchmarks
        if self.alternative_config_dir:
            for n in glob.glob(os.path.join(self.alternative_config_dir, self.BENCHMARKS_DIR, name + '.conf')):
                return BenchmarkToolConfiguration(n)
            for n in glob.glob(os.path.join(self.alternative_config_dir, self.BENCHMARKS_DIR, name + '.yaml')):
                return BenchmarkToolConfiguration(n)

        for n in glob.glob(os.path.join(self.default_config_dir, self.BENCHMARKS_DIR, name + '.conf')):
            return BenchmarkToolConfiguration(n)
        for n in glob.glob(os.path.join(self.default_config_dir, self.BENCHMARKS_DIR, name + '.yaml')):
            return BenchmarkToolConfiguration(n)

        # also look just below <config_dir>
        if self.alternative_config_dir:
            for n in glob.glob(os.path.join(self.alternative_config_dir, name + '.conf')):
                return BenchmarkToolConfiguration(n)
            for n in glob.glob(os.path.join(self.alternative_config_dir, name + '.yaml')):
                return BenchmarkToolConfiguration(n)

        for n in glob.glob(os.path.join(self.default_config_dir, name + '.conf')):
            return BenchmarkToolConfiguration(n)
        for n in glob.glob(os.path.join(self.default_config_dir, name + '.yaml')):
            return BenchmarkToolConfiguration(n)

        # finally see if the name of the benchmark is a valid file
        if os.path.exists(name):
            return BenchmarkToolConfiguration(name)

        raise ControllerConfigurationException('Benchmark with name {0} does not exist'.format(name))


    # TODO: add an alternative location (based on environment variable)
    def get_storage_config_file(self):
        """
        Returns the configuration file for the storage.
        :return: 
        """
        if self.alternative_config_dir:
            file = os.path.join(self.alternative_config_dir, self.STORAGE_CONFIG_FILE)
            if os.path.isfile(file):
                return file

            file = os.path.join(self.alternative_config_dir, self.STORAGE_JSON_CONFIG_FILE)
            if os.path.isfile(file):
                return file

        file = os.path.join(self.default_config_dir, self.STORAGE_CONFIG_FILE)
        if os.path.isfile(file):
            return file

        file = os.path.join(self.default_config_dir, self.STORAGE_JSON_CONFIG_FILE)
        if os.path.isfile(file):
            return file

        raise ControllerConfigurationException('Storage configuration file not found')

    def get_provider_config_file(self, name):

        if os.path.isfile(name):
            return name

        if self.alternative_config_dir:
            for f in os.listdir(os.path.join(self.alternative_config_dir, self.CLOUD_PROVIDERS_DIR)):
                if os.path.splitext(f)[0] == name:
                    return os.path.join(self.alternative_config_dir, self.CLOUD_PROVIDERS_DIR, f)


        for f in os.listdir(os.path.join(self.default_config_dir, self.CLOUD_PROVIDERS_DIR)):
            if os.path.splitext(f)[0] == name:
                return os.path.join(self.default_config_dir, self.CLOUD_PROVIDERS_DIR, f)

        raise ControllerConfigurationException('Impossible to find prodiver configuration for {0}'.format(name))

    def get_benchmark_config_file(self, name):

        if os.path.isfile(name):
            return name

        # first look within <config_dir>/benchmarks
        if self.alternative_config_dir:
            file = os.path.join(self.alternative_config_dir, self.BENCHMARKS_DIR, name + ".conf")
            if os.path.isfile(file):
                return file
            file = os.path.join(self.alternative_config_dir, self.BENCHMARKS_DIR, name + ".yaml")
            if os.path.isfile(file):
                return file

        file = os.path.join(self.default_config_dir, self.BENCHMARKS_DIR, name + ".conf")
        if os.path.isfile(file):
            return file
        file = os.path.join(self.default_config_dir, self.BENCHMARKS_DIR, name + ".yaml")
        if os.path.isfile(file):
            return file

        # also look just below <config_dir>
        if self.alternative_config_dir:
            file = os.path.join(self.alternative_config_dir, name + ".conf")
            if os.path.isfile(file):
                return file
            file = os.path.join(self.alternative_config_dir, name + ".yaml")
            if os.path.isfile(file):
                return file

        file = os.path.join(self.default_config_dir, name + ".conf")
        if os.path.isfile(file):
            return file
        file = os.path.join(self.default_config_dir, name + ".yaml")
        if os.path.isfile(file):
            return file

        raise ControllerConfigurationException('Impossible to find benchmark configuration for {0}'.format(name))


CONFIG_NESTING_PREFIXES = ['db', 'metrics_db', 'kafka', 'confservice', 'synchronizer', 'analytics']


class BaseBenchsuiteConfig(BaseModel):

    # only works at the first level (this is what we want actually, because also the nesting works only at the
    # first level)
    def flatten_dict(self):
        temp = self.dict()
        res = {}
        for k,v in temp.items():
            if isinstance(v, dict):
                for ik,iv in v.items():
                    new_k = f'{k}_{ik}'
                    res[new_k] = iv
            else:
                res[k] = v
        return res

    # only works at the first level
    @staticmethod
    def nest_params(params, prefixes=CONFIG_NESTING_PREFIXES):
        res = {}
        for k, v in params.items():
            copied = False
            for p in prefixes:
                tok = f'{p}_'
                if k.startswith(tok):
                    new_k = k[len(tok):]
                    if p in res:
                        d = res[p]
                    else:
                        d = {}
                        res[p] = d
                    d[new_k] = v
                    copied = True
                    break

            if not copied:
                res[k] = v
        return res

    def pretty_print(self):
        print("Config:\n-------")
        BaseBenchsuiteConfig.__pretty_print_dict(self.dict())

    @staticmethod
    def __pretty_print_dict(d, indent=0):
        for key, value in d.items():
            if isinstance(value, dict):
                print(' ' * indent + str(key) + ":")
                BaseBenchsuiteConfig.__pretty_print_dict(value, indent + 2)
            else:
                print(' ' * indent + str(key) + ": " + str(value))


class RemoveMeModel(BaseModel):
    name_a: str = ''
    pippo: Optional[int] = 0


class DBConfig(BaseModel):
    default_host: ClassVar = 'localhost'
    default_port: ClassVar = 27017
    default_name: ClassVar = 'benchsuite'
    default_username: ClassVar = None
    default_password: ClassVar = None
    default_aes_key: ClassVar = None
    default_providers_collection: ClassVar = 'providers'
    default_workloads_collection: ClassVar = 'workloads'
    default_users_collection: ClassVar = 'users'
    default_schedules_collection: ClassVar = 'scheduling'
    default_executions_collection: ClassVar = 'executions'
    default_requestor: ClassVar = None

    host: str = default_host
    port: int = default_port
    name: str = default_name
    username: Optional[str] = default_username
    password: Optional[str] = default_password
    aes_key: Optional[str] = default_aes_key
    providers_collection: str = default_providers_collection
    workloads_collection: str = default_workloads_collection
    users_collection: str = default_users_collection
    schedules_collection: str = default_schedules_collection
    executions_collection: str = default_executions_collection
    requestor: Optional[str] = default_requestor


class MetricsDBConfig(BaseModel):
    host: str = 'localhost'
    port: int = 8086
    name: Optional[str] = 'benchsuite'
    username: Optional[str] = None
    password: Optional[str] = None
    auth_token: Optional[str] = None


class ControllerConfig(BaseBenchsuiteConfig):
    default_verbose: ClassVar = 0
    default_backends_file: ClassVar = None

    verbosity: int = Field(0, click={'count': True, 'short_name': 'v'})
    quiet: bool = Field(False, click={'short_name': 'q', 'is_flag': True})
    config_dir: Optional[str] = os.path.join(user_config_dir(), 'benchmarking-suite')
    backends_file: str = default_backends_file

    db: DBConfig = None

    def legacy_get_provider_config_file(self, name):
        cc = ControllerConfiguration(alternative_config_dir=self.config_dir)
        return cc.get_provider_config_file(name)

    def legacy_load_benchmark_by_name(self, name):
        cc = ControllerConfiguration(alternative_config_dir=self.config_dir)
        return cc.get_benchmark_by_name(name)

