#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from datetime import datetime
from pprint import pprint

import plotille
from prettytable import PrettyTable


def fmt_execution_jobs(results, show_logs):
    for step, sstatus in results.status.steps.items():
        #print(f'Logs:\n{sstatus.logs if show_logs else "-- omitted --"}')
        for job, jstatus in sstatus.components.items():
            print(f'=== {step} === {job}')
            print(f'status: {jstatus.retval}')
            print(f'Stdout:\n{jstatus.stdout if show_logs else "-- omitted --"}')
            print(f'Stderr:\n{jstatus.stderr if show_logs else "-- omitted --"}')
            print('Profiles:')
            for p in jstatus.profiles:
                print(p['tags'])
                print(f't0={p["t0"]}')
                print(fmt_profile(p))
                print(fmt_profile_plot(p, 'cpu'))
                print(fmt_profile_plot(p, 'mem'))


def fmt_execution_results_steps(results):
    table = PrettyTable(align="l")
    table.field_names = ["Step", "Duration (s)", "Start Time", "Status"]
    for s, r in results.status.steps.items():
        start_time = datetime.fromtimestamp(r.start).strftime('%Y-%m-%d %H:%M:%S') if r.start else '--'
        if r.errno == 0:
            status = 'OK'
        else:
            status = f'ERROR ({r.exception})'
        duration = round(r.end - r.start, 2) if r.start and r.end else 'n/a'
        table.add_row([s, duration, start_time, status])
    return table.get_string()


def fmt_executions(executions):
    table = PrettyTable(align="l")
    table.field_names = ["Id", "Workload", "Created", "Params", "Session", 'Status']
    for e in executions:
        created = datetime.fromtimestamp(e.created).strftime('%Y-%m-%d %H:%M:%S')

        last_executed_step = e.status.get_last_executed_step()
        if last_executed_step == 'cleanup' and e.status.steps['cleanup'].errno == 0:
            overall_status = 'completed'
        elif last_executed_step == None:
            overall_status = 'new'
        elif last_executed_step:
            overall_status = last_executed_step
        else:
            overall_status = f'{"ERROR" if e.status.last_executed_step_status != 0 else ""} progress ({e.status.last_executed_step})'

        table.add_row([e.id, str(e.workload.tool)+'/'+str(e.workload.name), created, str(e.params), e.session.id, overall_status])

    return table.get_string()


def fmt_execution_results_metrics(execution):
    table = PrettyTable(align="l")
    table.field_names = ["Name", "Value", "Unit"]
    for s, r in execution.status.metrics.items():
        table.add_row([s,  r['value'], r['unit']])
    return table.get_string()


def fmt_session_list(sessions):
    table = PrettyTable(align="l")
    table.field_names = ["Id", "Providers", "Props"]
    for s in sessions:
        table.add_row([s.id,  list(s.providers.keys()), ', '.join([f'{k}={v}' for k,v in s.props.items()])])
    return table.get_string()


def fmt_profile(profile):
    if not profile['items']:
        return "Empty profile"
    fields = list(profile['items'][0].keys())
    # make sure time is at the beginning
    fields.remove('time')
    headers = [f'time (s)']
    headers.extend([f'{f} ({profile["units"][f]})' for f in fields])

    table = PrettyTable(field_names=headers, align='r')
    table.align['time'] = 'c'
    table.border = True

    for v in profile['items']:
        values = [v['time'] / 1000]
        values.extend([v[h] for h in fields])
        table.add_row(values)
    return table.get_string()


def fmt_profile_plot(profile, metric):
    if not profile['items']:
        return ""
    x = []
    y = []
    for v in profile['items']:
        x.append(v['time'] / 1000)
        y.append(v[metric])
    fig = plotille.Figure()
    fig.origin = False
    fig.width = 60
    fig.height = 10
    fig.color_mode = 'byte'
    fig.plot(x,y, lc=100, label='sin')
    return fig.show()


def fmt_execution_describe(execution, show_logs=False):
    print(f'\n***** Execution {execution.id} ({execution.workload.bsid}) *****')
    #print('\nStatus:\n======')
    #pprint(execution.status.dict())
    print('\nSteps:\n======')
    print(fmt_execution_results_steps(execution))
    print('\nJobs:\n======')
    fmt_execution_jobs(execution, show_logs)
    print('\nMetrics:\n======')
    print(fmt_execution_results_metrics(execution))


def fmt_workload_list_jobs(w):
    table = PrettyTable(align="l")
    table.field_names = ["Job", "Phase", "Type", "Profile", "SUT"]
    for j in w.jobs:
        table.add_row([j.name, j.phase, j.type, 'yes' if j.profile else '', 'yes' if j.is_sut() else ''])
    return table.get_string()