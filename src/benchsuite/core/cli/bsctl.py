#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import sys
from pprint import pprint
import click
import yaml

from benchsuite.core.cli.ascii_tables import fmt_executions, \
    fmt_session_list, fmt_execution_describe, fmt_workload_list_jobs
from benchsuite.core.cli.cli_config import options_from_model, options_from_model_2
from benchsuite.core.config import ControllerConfig
from benchsuite.core.controller import BenchmarkingController
from benchsuite.core.loaders.provider_loaders import load_provider
from benchsuite.core.loaders.workload_loaders import load_workloads
from benchsuite.core.util.logging import init_logging
from benchsuite.core.util.version import fmt_versions, container_version, modules_version

logger = logging.getLogger(__name__)

@click.group()
@options_from_model(ControllerConfig)
@options_from_model_2(ControllerConfig)
@click.pass_context
def cli(ctx, model, **kwargs):

    init_logging(-1 if model.quiet else model.verbosity)
    logger.info(f'Multiexec version: {container_version()}')
    logger.info(f'Installed modules: {modules_version()}')
    ctx.obj = ctx.with_resource(BenchmarkingController(config=model))


@click.group()
def session():
    pass

@click.command(name='version')
def version_cmd():
    print(fmt_versions())

@click.command()
@click.option('--provider', multiple=True)
@click.option('--workload', multiple=True)
@click.option('--prov-attr', multiple=True)
@click.option('--prop', multiple=True)
@click.option('--prov-prop', multiple=True)
@click.option('--play-now', default=False, is_flag=True)
@click.option('--show-logs', default=False, is_flag=True)
@click.option('--skip-store', default=False, is_flag=True)
@click.option('--skip-delete-schedule-old-resources', default=False, is_flag=True)
@click.option('--dry-run', default=False, is_flag=True)
@click.pass_obj
@click.pass_context
def create(ctx, obj, provider, workload, prov_attr, play_now, prop, prov_prop, show_logs, skip_store, dry_run, skip_delete_schedule_old_resources):
    s = obj.session_create(providers=provider, workloads=workload, prov_attrs=prov_attr, props=prop, prov_props=prov_prop)
    if play_now:
        ctx.invoke(session_play, session_id=s.id, show_logs=show_logs, skip_store=skip_store, dry_run=dry_run, skip_delete_schedule_old_resources=skip_delete_schedule_old_resources)


@click.command(name='list')
@click.pass_obj
def session_list(obj):
    sessions = obj.session_list()
    print(fmt_session_list(sessions))


@click.command('play')
@click.argument('session-id')
@click.option('--show-logs', default=False, is_flag=True)
@click.option('--skip-store', default=False, is_flag=True)
@click.option('--dry-run', default=False, is_flag=True)
@click.option('--skip-delete-schedule-old-resources', default=False, is_flag=True)
@click.pass_obj
@click.pass_context
def session_play(ctx, obj, session_id, show_logs, skip_store, dry_run, skip_delete_schedule_old_resources):
    obj.session_play(session_id, skip_store=skip_store, dry_run=dry_run, skip_delete_schedule_old_resources=skip_delete_schedule_old_resources)
    s = obj.get_session(session_id)
    if not obj.config.quiet:
        for e in s.list_executions():
            ctx.invoke(execution_describe, id=e.id, show_logs=show_logs) #  show_logs=obj.config.verbose > 2)

@click.command('cleanup')
@click.argument('session-id')
@click.pass_obj
def session_cleanup(obj, session_id):
    obj.session_cleanup(session_id)

@click.command('init')
@click.argument('session-id')
@click.pass_obj
def session_init(obj, session_id):
    obj.session_init(session_id)

@click.group()
def execution():
    pass


@click.command(name='list')
@click.pass_obj
def execution_list(obj):
    res = obj.list_executions()
    print(fmt_executions(res))


@click.command(name="provision")
@click.argument('id')
@click.pass_obj
def execution_provision(obj, id):
    obj.execution_get(id).run_step('provision')


@click.command(name="deprovision")
@click.argument('id')
@click.pass_obj
def execution_deprovision(obj, id):
    obj.execution_get(id).ensure_connectivity()
    obj.execution_get(id).run_step('deprovision')


@click.command(name="store")
@click.argument('id')
@click.pass_obj
def execution_store(obj, id):
    obj.execution_store(id)


@click.command(name="setup")
@click.argument('id')
@click.pass_obj
def execution_setup(obj, id):
    obj.execution_get(id).ensure_connectivity()
    obj.execution_get(id).run_step('setup')


@click.command(name="prepare")
@click.argument('id')
@click.pass_obj
def execution_prepare(obj, id):
    obj.execution_get(id).ensure_connectivity()
    obj.execution_get(id).run_step('prepare')


@click.command(name="run")
@click.argument('id')
@click.pass_obj
def execution_run(obj, id):
    obj.execution_get(id).ensure_connectivity()
    obj.execution_get(id).run_step('run')


@click.command(name="collect")
@click.argument('id')
@click.pass_obj
def execution_collect(obj, id):
    obj.execution_get(id).ensure_connectivity()
    obj.execution_get(id).run_step('collect')


@click.command(name="cleanup")
@click.argument('id')
@click.pass_obj
def execution_cleanup(obj, id):
    obj.execution_get(id).ensure_connectivity()
    obj.execution_get(id).run_step('cleanup')


@click.command(name="play")
@click.option('--up-to', default=None)
@click.option('--dry-run', default=False, is_flag=True)
@click.option('--skip-store', default=False, is_flag=True)
@click.argument('id')
@click.pass_obj
def execution_play(obj, id, up_to, dry_run, skip_store):
    obj.execution_play(id, up_to_step=up_to, dry_run=dry_run, skip_store=skip_store)


@click.command(name="describe")
@click.argument('id')
@click.option('--show-logs', default=False, is_flag=True)
@click.pass_obj
def execution_describe(obj, id, show_logs):
    describe_exec(obj, id, show_logs=show_logs)


def describe_exec(controller, exec_id, show_logs=False):
    logger.info(f'###BSM-DESCR### {exec_id} ### START ###')
    fmt_execution_describe(controller.execution_get(exec_id), show_logs=show_logs)
    logger.info(f'###BSM-DESCR### {exec_id} ### END ###')


@click.command(name="store")
@click.argument('id')
@click.pass_obj
def execution_store(obj, id):
    obj.execution_store(id)


@click.group()
def provider():
    pass


@click.command(name="get")
@click.argument('name')
@click.option('--check-connection', default=False, is_flag=True)
@click.pass_obj
def provider_get(obj, name, check_connection):
    p = load_provider(name, obj.config)
    pprint(p.model.dict())
    if check_connection:
        try:
            p.check_connection()
            print('Connection is OK!')
        except Exception as ex:
            raise ex


@click.command(name="deep-clean")
@click.argument('name')
@click.option('--prop', multiple=True)
@click.pass_obj
def provider_deep_clean(obj, name, prop):

    labels = {}
    for p in prop:
        k, v = [v.strip() for v in p.split('=', maxsplit=1)]
        labels[k] = v

    p = load_provider(name, obj.config)
    p.startup()
    p.deep_cleanup(labels=labels)


@click.group()
def workload():
    pass


@click.command(name="get")
@click.argument('name')
@click.option('--output', '-o', default=None)
@click.pass_obj
def workload_get(obj, name, output):
    p = load_workloads(name, obj.config)
    for w in p:
        if not output:
            pprint(w.get_describe_object())
            continue
        if output == 'yaml':
            print(yaml.dump(w.dict()))
            continue


@click.command(name="list")
@click.pass_obj
def workload_list(obj):
    res = obj.workload_list()
    for w in res:
        print('--------------------------------')
        print(f'{w.tool}\t{w.name}\t{w.version}\t{w.revision}\t{w.bsid}')
        print('Jobs:')
        print(fmt_workload_list_jobs(w))
        print('--------------------------------\n')

cli.add_command(version_cmd)

cli.add_command(session)
session.add_command(create)
session.add_command(session_list)
session.add_command(session_play)
session.add_command(session_cleanup)
session.add_command(session_init)


cli.add_command(execution)
execution.add_command(execution_list)
execution.add_command(execution_provision)
execution.add_command(execution_setup)
execution.add_command(execution_prepare)
execution.add_command(execution_run)
execution.add_command(execution_collect)
execution.add_command(execution_cleanup)
execution.add_command(execution_deprovision)
execution.add_command(execution_store)
execution.add_command(execution_play)
execution.add_command(execution_describe)
execution.add_command(execution_store)

cli.add_command(provider)
provider.add_command(provider_get)
provider.add_command(provider_deep_clean)

cli.add_command(workload)
workload.add_command(workload_get)
workload.add_command(workload_list)


def main(args=None):
    cli(auto_envvar_prefix='BS', args=args)


if __name__ == '__main__':
    main(args=sys.argv[1:])
