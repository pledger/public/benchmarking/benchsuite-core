#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from importlib.metadata import version


def container_version():
    try:
        with open('/ci_build_ref') as f:
            return ' '.join(f.readlines()).strip()
    except:
        return 'undefined'


def modules_version():
    modules = ['benchsuite.core', 'benchsuite.stdlib', 'benchsuite.backends', 'benchsuite.rest', 'benchsuite.api']
    res = {}
    for m in modules:
        try:
            res[m] = version(m)
        except:
            res[m] = None
    return res


def fmt_versions():
    res = f'Container\n\tBuild: {container_version()}\nModules:\n'
    for m, v in modules_version().items():
        res += f'\t{m}: {v}\n'
    return res