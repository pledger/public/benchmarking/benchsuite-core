#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import ast
import itertools
import random
from pprint import pprint


def pronounceable_random_string(tokens_length=3):
    # I omitted some letters I don’t like
    vowels = 'aiueo'
    consonants = 'bdfgjklmnprstvwxz'
    token_count = 0
    while 1:
        if tokens_length and token_count == tokens_length:
            yield '-'
            token_count = 0
        yield random.choice(consonants)
        yield random.choice(vowels)
        token_count += 1


def random_id():
    return ''.join(itertools.islice(pronounceable_random_string(), 20))


def eval_math_expression(expression, variables={}):
    """
    from https://stackoverflow.com/questions/38860682/evaluating-a-mathematical-expression-without-eval-on-python3, but
    we added ast.Name, ast.Load allowed types to allow specify variables in the expression
    :param expression:
    :return:
    """
    try:
        tree = ast.parse(expression, mode='eval')
    except SyntaxError:
        return  # not a Python expression
    if not all(isinstance(node, (ast.Expression, ast.UnaryOp, ast.unaryop, ast.BinOp, ast.operator, ast.Num, ast.Name, ast.Load)) for node in ast.walk(tree)):
        return    # not a mathematical expression (numbers and operators)
    return eval(compile(tree, filename='', mode='eval'), variables)