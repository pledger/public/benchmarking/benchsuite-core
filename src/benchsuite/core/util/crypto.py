#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import os
import base64
from Crypto.Cipher import AES
from Crypto import Random

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]

logger = logging.getLogger(__name__)


class AESCipher:

    def __init__(self, db_aes_key=None, **kwargs):
        if db_aes_key:
            self.key = db_aes_key.encode("utf8")
        else:
            if 'AES_KEY' in os.environ:
                self.key = os.environ['AES_KEY'].encode("utf8")
            else:
                self.key = None
            logger.error("AES_KEY not provided! Operation on the db that will need to encrypt/decrypt data will not "
                         "work properly!")

    def encrypt(self, raw):
        if raw is None:
            return None
        raw = pad(raw).encode("utf8")
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        if enc is None:
            return None
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return unpad(cipher.decrypt(enc[16:])).decode("utf8")
