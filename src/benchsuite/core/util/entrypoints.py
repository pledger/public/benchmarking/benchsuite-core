#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
from importlib import metadata

logger = logging.getLogger(__name__)


def get_workload_controller(workload_class):
    workload = get_workload_name_by_class(workload_class)

    for ep in metadata.entry_points()['benchsuite.workload.controller']:
        clazz = ep.load()
        if workload in clazz.SUPPORTED_WORKLOADS:
            logger.info('Found workload controller %s', clazz.__name__)
            return clazz

    raise Exception('WorkloadController not found!')


def get_provider_provisioners(provider_class, supported_execenv_types, prefer_execenv_type=None, force_execenv_type=None):
    provider = get_provider_name_by_class(provider_class)
    if 'PROVIDER_NAME' in provider_class.__dict__:
        logger.debug('Overriding provider name because the provider class defines the PROVIDER_NAME attribute')
        provider = provider_class.__dict__['PROVIDER_NAME']

    if force_execenv_type:
        if not force_execenv_type in supported_execenv_types:
            raise Exception('Provisioner not found! Tried to force execenv type to a type ({0}) not supported by the workload ({1})'.format(force_execenv_type, supported_execenv_types))
        logger.info(
            f'Searching a provisioner for provider "{provider}" and execution environment type "{force_execenv_type}" (forced by force_execenv_type prop)')
        supported_execenv_types = [force_execenv_type]
    else:
        logger.info(f'Searching a provisioner for provider "{provider}" and execution environment {supported_execenv_types}')

    res = []
    for ep in metadata.entry_points()['benchsuite.provider.provisioner']:
        clazz = ep.load()
        if provider in clazz.SUPPORTED_PROVIDERS and clazz.PROVISIONED_EXEC_ENV in supported_execenv_types:
            logger.info('Found provisioner %s', clazz.__name__)
            res.append(clazz)

    if not res:
        raise Exception('Provisioner not found!')

    if prefer_execenv_type:
        for c in res:
            if c.PROVISIONED_EXEC_ENV == prefer_execenv_type:
                logger.debug('Selecting provisioner %s because it is preferred (prefer_execenv_type prop)', c.__name__)
                return c
        logger.warning('Provisioner for the preferred execenv type not found!')

    if len(res) > 1:
        logger.debug('Multiple provisioner found. Returning the first one')
        return res[0]

    return res[0]


def get_provider_name_by_class(provider_class):
    provider_driver = None
    for ep in metadata.entry_points()['benchsuite.provider.controller']:
        clazz = ep.load()
        if clazz == provider_class:
            provider_driver = ep.name
            break

    return provider_driver


def get_workload_name_by_class(workload_class):
    workload_driver = None
    for ep in metadata.entry_points()['benchsuite.workload']:
        clazz = ep.load()
        if clazz == workload_class:
            workload_driver = ep.name
            break

    return workload_driver



def load_provider():
    pass