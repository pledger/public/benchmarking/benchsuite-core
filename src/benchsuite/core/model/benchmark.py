#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import os
import sys
import traceback
from importlib import metadata
from typing import List, Any, Optional, Dict

import yaml
from attrdict import AttrDict
from pydantic import BaseModel, Extra

from benchsuite.core.model.exception import ControllerConfigurationException

logger = logging.getLogger(__name__)


def merge_wokrload_keys(common_config, workload_config):

    for k, v in workload_config.items():
        if k not in common_config:
            common_config[k] = v
            continue

        if isinstance(v, dict) and isinstance(common_config[k], dict):
            common_config[k].update(v)
        elif isinstance(v, list) and isinstance(common_config[k], list):
            common_config[k].extend(v)
        else:
            if isinstance(v, dict) or isinstance(v, list):
                logger.error('Cannot merge key %s in common and workload configurations because they have different types (%s and %s). Overwriting the common value', k, type(common_config[k]), type(v))

            common_config[k] = v


def load_workloads_from_config_file(config_file, workload_name):
    if not os.path.isfile(config_file):
        raise ControllerConfigurationException('Config file {0} does not exist'.format(config_file))

    with open(config_file, 'r') as f:
        config = yaml.safe_load(f)
        if not workload_name or workload_name == '*':
            workloads = config['workloads'].keys()
        else:
            workloads = [workload_name]
        res = []
        for w in workloads:
            wc = dict(config)
            merge_wokrload_keys(wc, config['workloads'][w])
            if not wc.get('name', None):
                wc['name'] = w
            del wc['workloads']
            res.append(load_workload_from_dict(wc))
        return res


class InvalidWorkload(BaseModel):
    id: str


class BenchsuiteObject(BaseModel):

    tags: Optional[Dict[str, str]] = {}

    @property
    def bsid(self):
        raise NotImplementedError


class Workload(BenchsuiteObject):

    id: str
    name: str
    tool: str
    version: str
    description: str = ''
    categories: List[str] = []
    parser: Any = None

    @property
    def bsid(self):
        return tokens_to_wid(self.tool, self.name, self.version)

    def get_describe_object(self):
        return AttrDict({
            'name': self.name,
            'tool': self.tool,
            'version': self.version,
            'wid': self.bsid,
            'categories': self.categories,
            'description': self.description
        })

    def get_key_values_for_storage(self):
        return {
            'workload': self.name,
            'tool': self.tool,
            'version': self.version
        }


class WorkloadController:
    pass


def tokens_to_wid(tool, workload, version):
    wid = f'{tool}:{workload}:{version}'
    return wid.lower().replace(' ', '-')


def load_workload_from_dict(dict):

    # logger.debug('Loading workload from dict with id=%s, tool_name=%s, workload_name=%s', dict.get('id', None), dict.get('tool_name', None), dict.get('workload_name', None))

    # adapt the structure from workload as stored in db to the expected workload dict
    if 'workload_parameters' in dict:
        dict.update(dict['workload_parameters'])
        del dict['workload_parameters']

    workload_class = dict.get('class', None)
    driver = dict.get('driver', None)

    if 'id' not in dict:
        dict['id'] = f'{dict["tool"]}:{dict["name"]}:{dict["version"]}'

    if workload_class:
        # legacy way to find provider class
        module_name, class_name = workload_class.rsplit('.', 1)

        try:
            __import__(module_name)
            module = sys.modules[module_name]
            clazz = getattr(module, class_name)
            return clazz.load_from_dict(dict)
        except Exception as ex:
            logger.error('Error loading workload definition with id "%s": %s', dict['id'], str(ex))
            return InvalidWorkload.parse_obj(dict)

    elif driver:
        clazz = None
        for ep in metadata.entry_points()['benchsuite.workload']:
            if ep.name == driver:
                clazz = ep.load()
                break
        return clazz.load_from_dict(dict)
    else:
        raise Exception('No "class" or "driver" attributes found in the workload definition.')
