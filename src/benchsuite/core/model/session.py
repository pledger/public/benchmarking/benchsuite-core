#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging

from attrdict import AttrDict

from benchsuite.core.model.execution import Execution
from benchsuite.core.util.strings import random_id

logger = logging.getLogger(__name__)


class Session(object):

    def __init__(self):
        self.id = random_id()
        self.providers = {}
        self.workloads = []
        self.param_combination_sets = []
        self.executions = {}
        self.props = {}

    # -------------------------------------------------------------------------
    # Serialization

    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, state):
        self.__dict__ = state
    #
    # -------------------------------------------------------------------------

    def generate_executions(self):

        param_combination_sets = self.param_combination_sets if self.param_combination_sets else [{}]
        # TODO: make this method idempotent if called multiple times (do not recreate executions that already exists)
        # TODO: params could be in the form <provider-name>.<param-name> (e.g. a.nodes, b.profiler). If there is no
        # the namespace, it should be applied to all providers
        # TODO: generate_execution_params() should be called for EACH provider and results merged
        for pcs in param_combination_sets:
            for k, p in self.providers.items():
                exec_params = p.generate_execution_params(pcs)
                logger.debug('Execution params: %s', exec_params)
                for w in self.workloads:
                    for params in exec_params:
                        e = Execution(workload=w, session=self, params={'provider.default': params})
                        self.executions[e.id] = e

    def list_executions(self):
        return self.executions.values()

    def get_execution(self, exec_id):
        return self.executions[exec_id]

    def startup(self):
        for p in list(self.providers.values()):
            p.startup()

    def shutdown(self):
        for p in list(self.providers.values()):
            p.shutdown()

    def init(self, skip_delete_schedule_old_resources=False):

        # In case this session is created by the scheduler (it will add the schedule id in --prop schedule-id=123)
        # try to delete all resources that have the same schedule id. This should clean all resources from a previous
        # execution of the same schedule that were not delete successfully (e.g. the previous schedule execution failed)
        if 'schedule-id' in self.props and not skip_delete_schedule_old_resources:
            for p in list(self.providers.values()):
                p.deep_cleanup(labels={'benchsuite.session.schedule-id': self.props['schedule-id']})

    def cleanup(self):
        for p in list(self.providers.values()):
            p.cleanup()

    def get_describe_object(self):
        return AttrDict({
            'props': self.props
        })