#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
from abc import abstractmethod
from importlib import metadata

logger = logging.getLogger(__name__)


class Profiler(object):

    @abstractmethod
    def __init__(self, provider):
        raise NotImplementedError

    @abstractmethod
    def prepare_for_profiling(self, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def start_profiling(self, profile_tags, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def end_profiling(self, profile_job_id):
        raise NotImplementedError

    @abstractmethod
    def done_for_profiling(self, **kwargs):
        raise NotImplementedError


class NOOPProfiler(Profiler):

    def __init__(self, provider):
        pass

    def prepare_for_profiling(self, **kwargs):
        logger.info('Profiling is disabled')

    def start_profiling(self, profile_tags, **kwargs):
        logger.info('Profiling is disabled')

    def end_profiling(self, profile_job_id):
        logger.info('Profiling is disabled')

    def done_for_profiling(self, **kwargs):
        logger.info('Profiling is disabled')


class ExternalHTTPProfiler(Profiler):

    def __init__(self, provider):
        pass
        #self.start_url = start_url
        #self.stop_url = stop_url
        #self.get_profile_url = get_profile_url

    def start_profiling(self, profile_tags, **kwargs):
        payload = dict(kwargs)
        payload['tags'] = profile_tags
        print('Sending to profiler:', payload)
        pass

    def end_profiling(self, profile_job_id):
        logger.debug('Stopping profiling')

    def prepare_for_profiling(self, **kwargs):
        pass

    def done_for_profiling(self, **kwargs):
        pass
