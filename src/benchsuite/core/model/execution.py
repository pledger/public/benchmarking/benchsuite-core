#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import datetime
import logging
import time
import traceback
from abc import ABC, abstractmethod
from importlib import metadata
from pprint import pprint
from typing import Optional, Dict, Any, List, TypedDict

from attrdict import AttrDict
from pydantic import BaseModel

from benchsuite.core.model.benchmark import Workload
from benchsuite.core.model.executor import Executor
from benchsuite.core.util.entrypoints import get_workload_controller, \
    get_provider_provisioners
from benchsuite.core.util.logging import set_logging_context, start_logs_recording, stop_logs_recording
from benchsuite.core.util.strings import random_id, eval_math_expression

logger = logging.getLogger(__name__)


class ExecutionResultParser(ABC):

    def __init__(self, config=None, **kwargs):
        pass

    @abstractmethod
    def get_metrics(self, tool, workload, logs):
        """
        Parsers the execution output to extract metrics
        :param stdout:
        :param stderr:
        """
        pass


class ComponentStatus(BaseModel):

    stdout: str = None
    stderr: str = None
    retval: int = None
    node: str = None
    runtime: float = None
    profiles: List[dict] = []


class StepStatusComponents(TypedDict):
    name: str
    step_status: ComponentStatus


class StepStatus(BaseModel):

    errno: int = 0
    start: datetime.datetime = None
    end: datetime.datetime = None
    logs: List[str] = []
    exception: str = None
    traceback: str = None
    components: StepStatusComponents = {}


class ExecutionStatusSteps(TypedDict):
    name: str
    step_status: StepStatus


class ExecutionStatus(BaseModel):

    steps: ExecutionStatusSteps = {'provision': StepStatus(), 'setup': StepStatus(), 'prepare': StepStatus(), 'run': StepStatus(), 'collect': StepStatus(), 'cleanup': StepStatus(), 'deprovision': StepStatus()}
    sut_nodes: List[str] = []
    metrics: Dict = {}
    exec_envs: Dict = {}
    #last_failed_step: str = None
    #last_failed_step_errno: int = 0
    #last_failed_step_exception: Any = None

    def component_status_by_name(self, name):
        for _, v in self.steps.items():
            if name in v.components:
                return v.components[name]
        return None

    def get_all_component_statuses(self):
        res = []
        for _, s in self.steps.items():
            res.extend(list(s.components.values()))
        return res

    def get_all_profiles(self):
        res = []
        for c in self.get_all_component_statuses():
            res.extend(c.profiles)
        return res

    def get_last_executed_step(self):
        newest_step = None
        newest_date = 0
        for k, v in self.steps.items():
            if not v.start:
                continue
            if v.start > newest_date:
                newest_date = v.start
                newest_step = k
        return newest_step

    def failed_steps(self):
        return [s for s in self.steps.values() if s.errno != 0]


class Execution(BaseModel):

    workload: Workload
    session: Any  # FIXME: should be of type "Session", but it introduces a dependency loop
    provider: Any
    id: Optional[str] = None
    params: Dict = {}
    created: int = time.time()
    performance: float = None
    _workload_controller = None
    _provisioner = None
    context: Dict = {}
    status: ExecutionStatus = ExecutionStatus()

    class Config:
        underscore_attrs_are_private = True

    def __init__(self, **data: Any):
        super().__init__(**data)
        self._initialize()

    def _initialize(self):
        if not self.id:
            self.id = random_id()
        self.init_provisioner()

        self.context['props'] = {'session': dict(self.session.props)}

        # needed currently only because storage backends uses it to create record to store
        self.provider = self.session.providers['default']

    def init_provisioner(self):
        workload_class = self.workload.__class__
        clazz = get_workload_controller(workload_class)
        self._workload_controller = clazz(execution=self)

        provider_class = self.session.providers["default"].__class__


        # TODO: take force_exec_env and prefer_exec_env from session

        clazz = get_provider_provisioners(provider_class,
                                          self._workload_controller.get_supported_provisioned_types(),
                                          prefer_execenv_type=self.session.props.get('prefer_execenv_type'),
                                          force_execenv_type=self.session.props.get('force_execenv_type'))

        logger.debug('Initializing provisioner %s', clazz.__name__)
        self._provisioner = clazz(self.session.providers["default"])

    def run_step(self, step, dry_run=False, **kwargs):
        set_logging_context(execution=self.id, step=step, workload=self.workload.bsid)
        logger.info(f'---BMS---STEP-START---{self.id}---{step}---')
        start_logs_recording()

        function = getattr(self, f'_{step}')

        try:
            start_time = time.time()
            self.status.steps[step].start = start_time
            logger.debug('start time  is %s', self.status.steps[step].start)
            function(dry_run=dry_run, **kwargs)
            end_time = time.time()
        except Exception as ex:
            end_time = time.time()
            self.status.steps[step].exception = str(ex)
            self.status.steps[step].traceback = traceback.format_exc()
            self.status.steps[step].errno = 1
            logger.info(f'---BMS---ERR-START---{self.id}---{step}---')
            logger.error(f'Exception:\n{ex}\nTraceback:\n{traceback.format_exc()}')
            logger.info(f'---BMS---ERR-STOP---{self.id}---{step}---')
            raise ex
        finally:
            self.status.steps[step].end = end_time
            logger.debug('end time  is %s', self.status.steps[step].end)
            self.status.steps[step].logs = stop_logs_recording()
            logger.info(f'---BMS---STEP-STOP---{self.id}---{step}---')
            set_logging_context(execution=None, step=None, workload=None)

    def _provision(self, dry_run=False, **kwargs):
        hard = kwargs.get('hard', True)
        prov = self._provisioner.provision(self.params['provider.default'], self.context, hard=hard, dry_run=dry_run)
        self.context['execenvs'] = {'default': prov}
        self._workload_controller.provider_default = prov
        self.status.exec_envs['provider.default'] = prov.model

    def ensure_connectivity(self):
        self._provisioner.ensure_connectivity(self._workload_controller.provider_default)

    def _deprovision(self, dry_run=False, **kwargs):
        if dry_run:
            return
        hard = kwargs.get('hard', True)
        self._provisioner.deprovision(hard=hard)
        self._workload_controller.provider_default = None
        #del self.status.exec_envs['provider.default']

    def _setup(self, dry_run=False):
        self._workload_controller.setup(dry_run=dry_run)

    def _prepare(self, dry_run=False):
        self._workload_controller.prepare(dry_run=dry_run)

    def _run(self, dry_run=False):
        self._workload_controller.run(dry_run=dry_run)

    def _collect(self, dry_run=False):
        self._workload_controller.collect(dry_run=dry_run)
        # TODO: remove the first one and also the second one when the new execution model has been defined
        executor_info = self._workload_controller.get_describe_object()
        self.status.sut_nodes = executor_info.sut_nodes
        for k, v in executor_info.components.items():
            c = ComponentStatus(stdout=v['stdout'], stderr=v['stderr'], retval=v['ret_val'], node=v['node'], runtime=v['runtime'])
            self.status.steps[v['step']].components[k] = c
        for p in executor_info.profiles:
            comp = p['tags']['job']
            self.status.component_status_by_name(comp).profiles.append(p)

        if len(self.status.failed_steps()) > 0:
            logger.warning('Not trying to extract metrics since there are failed steps and metrics could be not reliables')
            return

        # NEW JB WORKLOADS
        try:
            performance = None
            if hasattr(self.workload, 'jobs'):
                self.status.metrics = {}
                parser_jobs = self.workload.jobs.filter(has_parser=True)
                for j in parser_jobs:
                    if self.status.component_status_by_name(j.name):
                        self.status.metrics.update(j.parser.get_metrics(self.status.component_status_by_name(j.name).stdout, self.status.component_status_by_name(j.name).stderr, self))

                if self.workload.performance_index:
                    # performance index defined. Try to calculate it
                    variables = {k: v['value'] for k, v in self.status.metrics.items()}
                    logger.debug('Found performance index definition: %s. Trying to calculate it', self.workload.performance_index)
                    try:
                        performance_index_value = eval_math_expression(self.workload.performance_index, variables)
                        performance_metric = {'value': performance_index_value, 'unit': None}
                    except Exception as ex:
                        logger.error('Error computing performance index: %s', ex)
                        performance_metric = None
                    if performance_metric:
                        performance = performance_metric['value']
                    else:
                        logger.warning('Couldn\'t calculate performance_index = %s because needed metric(s) does not exists', self.workload.performance_index)
                        performance = None
                    logger.info('Using metric %s as performance metric. performance=%s', self.workload.performance_index, performance)
                else:
                    # use runtime of the run-0 container
                    if self.status.component_status_by_name(self.workload.run_components_name()[0]).retval == 0:
                        if self.status.component_status_by_name(self.workload.run_components_name()[0]).runtime > 0:  # can be 0 for very short commands (e.g. echo "hello world")
                            performance = 1 / self.status.component_status_by_name(self.workload.run_components_name()[0]).runtime
                            logger.info('Using inverse of runtime of the first run component as performance metric. performance=%s', performance)
            else:
                if self.status.component_status_by_name(self.workload.run_components_name()[0]).retval == 0:
                    performance = 1 / self.status.component_status_by_name(self.workload.run_components_name()[0]).runtime
                    logger.info('Using inverse of runtime of the first run component as performance metric. performance=%s', performance)

            if performance:
                self.status.metrics['performance_index'] = {'value': performance, 'unit': None}
        except Exception as ex:
            logger.error('An exception occurred during metrics parsing. Metrics will not be available: %s', str(ex))
            print(traceback.format_exc())
            self.status.metrics = {}
            raise ex

    def _cleanup(self, dry_run=False):
        self._workload_controller.cleanup(dry_run=dry_run)
