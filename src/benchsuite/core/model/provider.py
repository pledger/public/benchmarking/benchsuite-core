#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import configparser
import itertools
import json
import os
import sys
from abc import abstractmethod
from importlib import metadata
from typing import List

import yaml
from attrdict import AttrDict

from benchsuite.core.model.benchmark import BenchsuiteObject
from benchsuite.core.model.exception import ControllerConfigurationException
from benchsuite.core.util.misc import dict_deep_merge


class ProviderMixin(object):

    @abstractmethod
    def initialize_mixin(self):
        raise NotImplementedError()

    @abstractmethod
    def startup_mixin(self):
        raise NotImplementedError()

    @abstractmethod
    def shutdown_mixin(self):
        raise NotImplementedError()

    @abstractmethod
    def cleanup_mixin(self):
        raise NotImplementedError()


class Provider():

    def __init__(self, model=None):
        self.name = None
        self.id = None
        self.metadata = {}
        self.props = {}

        if model:
            self.model = model
            self.name = self.model.name
            self.id = self.model.id
            self.metadata = self.model.metadata
            self.props = self.model.props

        self.initialize_mixins()

    @property
    def tags(self):
        return self.model.tags

    @property
    def bsid(self):
        return self.model.name

    def startup(self):
        self.startup_mixins()

    def shutdown(self):
        self.shutdown_mixins()

    def cleanup(self):
        """
        remove all objects created by this provider instance
        :return:
        """
        self.cleanup_mixins()

    @abstractmethod
    def pre_process_execution_params_combination(self, orig_params):
        raise NotImplementedError()

    def generate_execution_params(self, param_combination_set):
        if not self.model.execution_params_combination:
            return [{}]

        params = self.pre_process_execution_params_combination(self.model.execution_params_combination)
        # transform any value in list if not already a list
        params = {k: (v if isinstance(v, list) else [v]) for k, v in params.items()}

        combinations = list(itertools.product(*params.values()))
        keys = params.keys()
        res = []
        for c in combinations:
            exec_p = {}
            for i, k in enumerate(keys):
                exec_p[k] = c[i]
            res.append(exec_p)
        return res

    @abstractmethod
    def deep_cleanup(self):
        """
        tries to remove all objects created by the benchsuite (not only by this provider instance
        :return:
        """
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def build_from_dict(model):
        raise NotImplementedError()

    def initialize_mixins(self):
        for mixin in [b for b in self.__class__.__bases__ if b.__base__ == ProviderMixin]:
            mixin.initialize_mixin(self)

    def startup_mixins(self):
        for mixin in [b for b in self.__class__.__bases__ if b.__base__ == ProviderMixin]:
            mixin.startup_mixin(self)

    def shutdown_mixins(self):
        for mixin in [b for b in self.__class__.__bases__ if b.__base__ == ProviderMixin]:
            mixin.shutdown_mixin(self)

    def cleanup_mixins(self):
        for mixin in [b for b in self.__class__.__bases__ if b.__base__ == ProviderMixin]:
            mixin.cleanup_mixin(self)

    def get_describe_object(self):
        return AttrDict({
            'name': self.name,
            'id': self.id,
            'metadata': self.metadata
        })

    @abstractmethod
    def check_connection(self):
        raise NotImplementedError()


def load_service_provider_from_config_file(config_file, service_type=None, attrs_overrides=None):
    if not os.path.isfile(config_file):
        raise ControllerConfigurationException('Config file {0} does not exist'.format(config_file))

    try:
        with open(config_file) as f:
            config = configparser.ConfigParser()
            config.read_dict(json.load(f))
            config = config['provider']
    except ValueError as ex:
        try:
            with open(config_file, 'r') as f:
                config = yaml.safe_load(f)
        except:
            config = configparser.ConfigParser()
            config.read(config_file)
            config = dict(config.items('provider'))

    if 'name' not in config:
        config['name'] = os.path.splitext(os.path.basename(config_file))[0]

    return load_provider_from_dict(config, attrs_overrides=attrs_overrides)


def load_provider_from_dict(dict, attrs_overrides=None):
    provider_class = dict.get('class', None)
    driver = dict.get('driver', None)

    if 'id' not in dict:
        dict['id'] = dict['name']

    if attrs_overrides:
        dict_deep_merge(dict, attrs_overrides, override_on_conflict=True)

    if provider_class:
        # legacy way to find provider class
        module_name, class_name = provider_class.rsplit('.', 1)

        __import__(module_name)
        module = sys.modules[module_name]
        clazz = getattr(module, class_name)

        return clazz.load_from_dict(dict)
    elif driver:
        clazz = None
        for ep in metadata.entry_points()['benchsuite.provider.controller']:
            if ep.name == driver:
                clazz = ep.load()
                break
        return clazz.load_from_dict(dict)
    else:
        raise Exception('No "class" or "driver" attributes found in the provider definition.')


class ProviderProvisioner:

    SUPPORTED_PROVIDERS = []
    PROVISIONED_TYPE = ''

    def __init__(self, provider):
        self.provider = provider

    def provision(self, execution_params, execution_context, hard=True):
        raise NotImplementedError()

    def ensure_connectivity(self, exec_env):
        raise NotImplementedError()

    def deprovision(self, hard=True):
        raise NotImplementedError()


class ExecutionEnvironmentModel(BenchsuiteObject):

    bsid_tags: List[str]

    @property
    def bsid(self):
        return ';'.join([f'{k}:{v}' for k, v in self.tags.items() if k in self.bsid_tags])


class ExecutionEnvironment:

    BSID_TAGS = []
    TYPE = 'DefineInSubclasses'

    #
    # bsid_tags are which tags of the execution environment will be used to build the id. It is used to
    # aggregate results in Grafana
    #
    def __init__(self, tags={}, bsid_tags=None):
        self.model = ExecutionEnvironmentModel(tags=tags, bsid_tags=bsid_tags)
        self.model.tags['type'] = self.TYPE

    def ensure_connectivity(self):
        pass
