#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import datetime
from typing import List, Any

from pydantic import BaseModel, Field


class SchedulingHints(BaseModel):
    interval: dict[str, int] = {}
    onetime: bool = True
    at_time: datetime.time = None
    after: datetime.time = None
    before: datetime.time = None
    run_immediately: bool = True


class Schedule(BaseModel):

    id: str = None
    name: str = None
    interval: dict = {}
    scheduling_hints: SchedulingHints = SchedulingHints()
    username: str = None
    tags: List[str] = []
    properties: dict = {}
    tests: List[str] = []
    active: bool = False
    env: dict = {}
    provider_id: str = None
    service_types: List[str] = []
    metadata: dict = {}
    grants: dict = {}
    provider_config_secret: str = None
    benchsuite_additional_opts: List[str] = []
    docker_additional_opts: dict = {}
    executor: Any = None
    reports_scope: str = Field(alias='reports-scope', default=None)
    workload_parameters: dict = {}

    created_time: datetime.datetime = None
    updated_time: datetime.datetime = None


