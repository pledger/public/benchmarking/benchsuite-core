#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import glob
import os
import pprint
import logging
import traceback

from benchsuite.core.cli.ascii_tables import fmt_executions
from benchsuite.core.config import ControllerConfiguration
from benchsuite.core.model.exception import ControllerConfigurationException, UndefinedExecutionException
from benchsuite.core.model.session import Session
from benchsuite.core.model.storage import load_storage_connector_from_config_file, load_storage_connector_from_config_string
from benchsuite.core.loaders.provider_loaders import load_provider
from benchsuite.core.sessionmanager import SessionStorageManager
from benchsuite.core.cli.cli_config import comma_separated_list
from benchsuite.core.loaders.workload_loaders import load_workloads
from benchsuite.core.util.logging import set_logging_context
from benchsuite.core.util.misc import dict_deep_merge

CONFIG_FOLDER_ENV_VAR_NAME = 'BENCHSUITE_CONFIG_FOLDER'
DATA_FOLDER_ENV_VAR_NAME = 'BENCHSUITE_DATA_FOLDER'
PROVIDER_STRING_ENV_VAR_NAME = 'BENCHSUITE_PROVIDER'
SERVICE_TYPE_STRING_ENV_VAR_NAME = 'BENCHSUITE_SERVICE_TYPE'
STORAGE_CONFIG_FILE_ENV_VAR = 'BENCHSUITE_STORAGE_CONFIG'


logger = logging.getLogger(__name__)


class BenchmarkingController:
    """The facade to all Benchmarking Suite operations"""

    def __init__(self, config_folder=None, storage_config_file=None, config=None):

        self.config = config
        logger.info('BenchsuiteController started with config: %s',
                    pprint.pformat(config, indent=2, sort_dicts=False))
        config_folder = config.config_dir

        if not storage_config_file:
            storage_config_file = self.config.backends_file

        if not config_folder and CONFIG_FOLDER_ENV_VAR_NAME in os.environ :
            config_folder = os.environ[CONFIG_FOLDER_ENV_VAR_NAME]

        self.configuration = ControllerConfiguration(config_folder)

        data_folder = self.configuration.get_default_data_dir()
        if DATA_FOLDER_ENV_VAR_NAME in os.environ:
            data_folder = os.environ[DATA_FOLDER_ENV_VAR_NAME]

        self.session_storage = SessionStorageManager(data_folder)
        self.session_storage.load()

        try:
            # different ways to load the storage configuration:
            # 1. use the storage_config_file argument if initialized (the -r option in the CLI)
            # 2. use the content of the BENCHSUITE_STORAGE_CONFIG environment variable
            # 3. use the default location in the configuration folder

            if storage_config_file:
                logger.info('Loading storage configuration from file ' + storage_config_file)
                self.results_storage = load_storage_connector_from_config_file(storage_config_file)
            elif STORAGE_CONFIG_FILE_ENV_VAR in os.environ:
                logger.info('Loading storage configuration from {0} env variable'.format(STORAGE_CONFIG_FILE_ENV_VAR))
                self.results_storage = load_storage_connector_from_config_string(os.environ[STORAGE_CONFIG_FILE_ENV_VAR])
            else:
                logger.info('Loading storage configuration from default location ' + self.configuration.get_storage_config_file())
                self.results_storage = load_storage_connector_from_config_file(self.configuration.get_storage_config_file())

        except ControllerConfigurationException:
            logger.warning('Results storage configuration file not found. Results storage of results is disabled')
            self.results_storage = [] # None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        for s in self.session_list():
            s.shutdown()
        self.session_storage.store()
        return exc_type is None

    def session_list(self):
        return self.session_storage.list()

    def _props_args_split(self, props):
        if not props:
            return None
        res = {}
        for p in props:
            k, v = [v.strip() for v in p.split('=', maxsplit=1)]
            as_list = comma_separated_list(v)
            if len(as_list) > 1:
                v = as_list
            k_tokens = k.split('.')
            if len(k_tokens) == 1:
                res[k] = v
            else:
                t = {}
                tt = t
                for k in k_tokens[:-1]:
                    tt[k] = {}
                    tt = tt[k]
                tt[k_tokens[-1]] = v
                dict_deep_merge(res, t)

        return res

    def session_create(self, providers=None, workloads=None, prov_attrs=None, props=None, prov_props=None, exec_props=None):
        """

        :param providers:
        :param workloads:
        :param prov_attrs: override attributes of the provider
        :param props: session properties
        :param prov_props:  add props to provider
        :param exec_props:
        :return:
        """
        session = Session()

        if providers:
            prov_instances = {}
            for p in providers:
                try:
                    tokens = p.split('=', maxsplit=1)
                    if len(tokens) == 1:
                        tokens.insert(0, 'default')
                    # TODO: only props for this provider. The props can be namespaced (e.g. <provider_name>:<prop> - the dot is already used for nested attrs)
                    # TODO: if no namespace is provided, it should apply to all providers
                    prov = load_provider(tokens[1], self.config, attrs_overrides=self._props_args_split(prov_attrs))
                    prov_instances[tokens[0]] = prov
                except Exception as ex:
                    logger.error('Provider loading failed for "%s": %s', p, ex)
                    raise ex

            session.providers.update(prov_instances)

        if prov_props:
            prov_props = self._props_args_split(prov_props)
            # TODO: props could be scoped to be applied only to one provider (e.g. a.my_prop)
            for _, p in prov_instances.items():
                p.props.update(prov_props)

        if workloads:
            workload_instances = []
            for w in workloads:
                try:
                    workload_instances.extend(load_workloads(w, self.config))
                except Exception as ex:
                    logger.error('Provider loading failed for "%s": %s', w, ex)
                    raise ex

            session.workloads.extend(workload_instances)

        #if prov_params:
        #    combination_set = {}
        #    for p in prov_params:
        #        k, v = p.split('=', maxsplit=1)
        #        as_list = comma_separated_list(v)
        #        if len(as_list) > 1:
        #            v = as_list
        #        combination_set[k] = v
        #    session.param_combination_sets.append(combination_set)

        if props:
            new_props = {}
            for p in props:
                k, v = p.split('=', maxsplit=1)
                new_props[k.strip()] = v.strip()
            session.props.update(new_props)

        session.generate_executions()
        self.session_storage.add(session)
        logger.info(f'Session %s created', session.id)
        return session

    def session_play(self, session_id, skip_store=False, dry_run=False, skip_delete_schedule_old_resources=False):
        s = self.get_session(session_id)
        to_execute = list(s.list_executions())

        logger.info('Playing session. Executions that will be executed:\n%s', fmt_executions(to_execute))

        last_executed = None

        try:
            s.init(skip_delete_schedule_old_resources=skip_delete_schedule_old_resources)

            while to_execute:

                # 1. get next execution
                e = self._get_next_preferred_execution(last_executed, to_execute) or to_execute[0]

                # 2. run provision step
                self.execution_run_step(e, 'provision', hard=False, dry_run=dry_run)

                # 3. run all other steps
                self.execution_play(e.id, provision_delegated=True, dry_run=dry_run, skip_store=skip_store)

                # 4. run the deprovision step
                last_executed = e
                to_execute.remove(e)
                hard = not self._can_provisioned_be_reused(e, to_execute)
                logger.info('Can the provisioned environment be reused? %s', not hard)
                self.execution_run_step(e, 'deprovision', hard=hard, dry_run=dry_run)

                # 5. store results (this must be the last one to include results from all the other steps)
                if not skip_store and not dry_run:
                    self.execution_store(e.id)
        finally:
            s.cleanup()

    def _can_provisioned_be_reused(self, last_executed, to_execute):
        return self._get_next_preferred_execution(last_executed, to_execute) is not None

    def _get_next_preferred_execution(self, last_executed, to_execute):
        if last_executed:
            for e in to_execute:
                if e.params['provider.default'] == last_executed.params['provider.default']:
                    logger.info('Next preferred execution selected: %s', e.id)
                    return e
        logger.info('No next preferred execution found')
        return None

    def session_cleanup(self, session_id):
        s = self.get_session(session_id)
        s.cleanup()

    def session_init(self, session_id):
        s = self.get_session(session_id)
        s.init()

    def execution_run_step(self, execution, step, dry_run=False, **kwargs):
        if isinstance(execution, str):
            execution = self.execution_get(execution)

        # run the execution step
        try:
            execution.run_step(step, dry_run=dry_run, **kwargs)
        except Exception as ex:
            raise ex

    def execution_play(self, execution_id, provision_delegated=False, up_to_step=None, skip_store=False, dry_run=False):
        e = self.execution_get(execution_id)

        steps = ['provision', 'setup', 'prepare', 'run', 'collect', 'cleanup', 'deprovision']
        if provision_delegated:
            steps = steps[1:-1]

        no_exception = False
        try:
            for s in steps:
                self.execution_run_step(e, s, dry_run=dry_run)
                if s == up_to_step:
                    break
            no_exception = True
        except Exception as ex:
            logger.error('An error occurred while execution play: %s', str(ex))
            logger.info('Trying to collect status and logs and cleanup the execution')
            try:
                e.run_step('collect', dry_run=dry_run)
            except Exception as inner_ex:
                # catching this exception to be able to run the cleanup step
                logger.error('An error occurred while collecting results after an exception during execution play: %s', str(inner_ex))
            finally:
                e.run_step('cleanup', dry_run=dry_run)
                if not skip_store:
                    self.execution_store_error(e.id)

        if not up_to_step and not skip_store and no_exception and not dry_run and not provision_delegated:
            self.execution_store(execution_id)

    def execution_get(self, execution_id):
        for s in self.session_storage.list():
            try:
                return s.get_execution(execution_id)
            except:
                pass

        raise UndefinedExecutionException(f'Execution with id={execution_id} does not exist')

    def execution_store(self, execution_id):
        e = self.execution_get(execution_id)
        logger.info(f'###BSM-STORE### {e.id} ### START ###')

        for s in self.results_storage:
            try:
                s.save_execution_result(e)
            except Exception as ex:
                logger.error('Impossible to store results: %s', str(ex))
                logger.info(f'###BSM-EXC### {e.id} ### START ###')
                logger.error(f'Exception:\n{ex}\nTraceback:\n{traceback.format_exc()}')
                logger.info(f'###BSM-EXC### {e.id} ### END ###')

        logger.info(f'###BSM-STORE### {e.id} ### END ###')

    def execution_store_error(self, execution_id):
        e = self.execution_get(execution_id)
        for s in self.results_storage:
            s.save_execution_error(e)

    def workload_list(self):
        res = []
        for n in glob.glob(os.path.join(self.config.config_dir, 'workloads', '*.yaml')):
            w = load_workloads(f'file://{n}', self.config)
            res.extend(w)
        return res

    def list_sessions(self):
        return self.session_storage.list()

    def get_session(self, session_id: str):
        return self.session_storage.get(session_id)

    def destroy_session(self, session_id: str) -> None:
        s = self.get_session(session_id)
        logger.debug('Session loaded: {0}'.format(s))
        s.destroy()
        self.session_storage.remove(s)

    def list_executions(self):
        return [item for sublist in self.list_sessions() for item in sublist.list_executions()]
