#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import traceback
from abc import abstractmethod
from importlib import metadata
import re

from benchsuite.core.model.benchmark import load_workloads_from_config_file

logger = logging.getLogger(__name__)


regex = r"^((.+?):\/\/)?(.*?)(:(.+?))?$"


def load_workloads(workload_string, controller_config):
    match = re.match(regex, workload_string)
    protocol = match.group(2)
    name = match.group(3)
    qualifier = match.group(5)
    logger.debug(f'Searching loader for workload "{workload_string}" (protocol={protocol or ""}, name={name}, qualifier={qualifier or ""})')

    loaders = []
    if not protocol:
        loaders.append(LegacyWorkloadLoader)
    else:
        for ep in metadata.entry_points()['benchsuite.workload.loader']:
            if ep.name == protocol or ep.name.startswith(f'{protocol}:'):
                loaders.append(ep.load())
        logger.debug(f'Found following loaders: {[l.__module__ + "." + l.__qualname__ for l in loaders]}')

    workloads = []
    for loader in loaders:
        try:
            workloads.extend(loader(name, qualifier, controller_config).load())
            break
        except Exception as ex:
            logger.warn('Loader %s failed to load the workload "%s": %s', loader.__name__, name, ex)
            print(traceback.format_exc())
            continue

    if not workloads:
        raise Exception(f'Impossible to load the workload "{workload_string}": all loaders failed')
    return workloads


class WorkloadLoader(object):

    def __init__(self, workload_string, qualifier, controller_config):
        self.workload_string = workload_string
        self.qualifier = qualifier
        self.controller_config = controller_config

    @abstractmethod
    def load(self):
        raise NotImplementedError()


class LegacyWorkloadLoader(WorkloadLoader):

    def load(self):
        benchmark = self.controller_config.legacy_load_benchmark_by_name(self.workload_string)
        return load_workloads_from_config_file(benchmark.config_file, self.qualifier)
