#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import traceback
from abc import abstractmethod
from importlib import metadata
import re

from benchsuite.core.model.provider import load_service_provider_from_config_file

logger = logging.getLogger(__name__)


regex = r"^((.+?):\/\/)?(.*?)(:(.+?))?$"


def load_provider(provider_string, controller_config, attrs_overrides=None):
    match = re.match(regex, provider_string)
    protocol = match.group(2)
    name = match.group(3)
    qualifier = match.group(5)
    logger.debug(f'Searching loader for provider "{provider_string}" (protocol={protocol or ""}, name={name}, qualifier={qualifier or ""})')

    loaders = []
    if not protocol:
        loaders.append(LegacyProviderLoader)
    else:
        for ep in metadata.entry_points()['benchsuite.provider.loader']:
            if ep.name == protocol or ep.name.startswith(f'{protocol}:'):
                loaders.append(ep.load())
        logger.debug(f'Found following loaders: {[l.__module__ + "." + l.__qualname__ for l in loaders]}')

    provider = None
    for loader in loaders:
        try:
            provider = loader(name, qualifier, controller_config, attrs_overrides=attrs_overrides).load()
            break
        except Exception as ex:
            logger.warn('Loader %s failed to load the provider "%s": %s', loader.__name__, name, ex)
            print(traceback.format_exc())
            continue

    if not provider:
        raise Exception(f'Impossible to load the provider "{provider_string}": all loaders failed')
    return provider


class ProviderLoader(object):

    def __init__(self, provider_string, qualifier, controller_config, attrs_overrides=None):
        self.provider_string = provider_string
        self.qualifier = qualifier
        self.controller_config = controller_config
        self.attrs_overrides = attrs_overrides

    @abstractmethod
    def load(self):
        raise NotImplementedError()


class LegacyProviderLoader(ProviderLoader):

    def load(self):
        config_file = self.controller_config.legacy_get_provider_config_file(self.provider_string)
        return load_service_provider_from_config_file(config_file, self.qualifier, attrs_overrides=self.attrs_overrides)