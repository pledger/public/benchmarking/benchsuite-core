#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

import os
from pymongo import MongoClient

from benchsuite.core.db.mongo import MongoConnectorBase

logger = logging.getLogger(__name__)

class MongoDBUsersConnector(MongoConnectorBase):

    def __init__(self, db_host='localhost', db_port=27017, db_name='benchsuite', db_username=None, db_password=None, db_aes_key=None,
                 db_users_collection='scheduling', **kwargs):
        super().__init__(db_host, db_port, db_name, db_username, db_password, db_aes_key, **kwargs)
        self.user_collection = self.db[db_users_collection]

    def add_user(self, user):
        r = self.__create_user(user)
        self.user_collection.insert_one(r)
        return r

    def update_user(self,user):
        updated = self.__create_user(user)
        new_value = {"$set": updated}
        self.user_collection.replace_one({"id":user['id']}, new_value)
        return updated
        
    def user_exists(self, user_id):
        return self.user_collection.count_documents({"id":user_id})==1
        
    def list_users(self):
        out = []
        for user in self.user_collection.find():
            out.append(user)
        return out

    def list_organization_users(self, organization_id):
        out = []
        for user in self.user_collection.find({"organizations": organization_id}):
            out.append(user)
        return out

    def get_user(self, user_id):
        return self.user_collection.find_one({"id":user_id})

    def delete_user(self, user_id):
        return self.user_collection.delete_one({"id":user_id})
        
    def add_organization_membership(self, org_id, user_id):
        if not self.organization_exists(org_id):
            return None
        if not self.user_exists(user_id):
            return None
        user = self.get_user(user_id)
        if not org_id in user["organizations"]:
            user["organizations"].append(org_id)
        updated = {'organizations':user["organizations"]}
        new_value = {"$set": updated}
        self.user_collection.replace_one({"id":user_id}, new_value)
        return self.get_user(user_id)

    def delete_organization_membership(self, org_id, user_id):
        if not self.organization_exists(org_id):
            return None
        if not self.user_exists(user_id):
            return None
        user = self.get_user(user_id)
        if org_id in user["organizations"]:
            user["organizations"].remove(org_id)
            updated = {'organizations':user["organizations"]}
            new_value = {"$set": updated}
            self.user_collection.replace_one({"id":user_id}, new_value)
        return self.get_user(user_id)
        
    def __create_user(self, user):
        print(user)
        record = {
            'id': user['id'],
            'givenName': user['givenName'],
            'familyName': user['familyName'],
            'organizations': user['organizations']
        }

        return record

    def list_organizations(self):
        out = []
        for org in self.org_collection.find():
            out.append(org)
        return out

    def add_organization(self, organization):
        r = self.__create_organization(organization)
        self.org_collection.insert_one(r)
        return r

    def get_organization(self, organization_id):
        return self.org_collection.find_one({"id":organization_id})

    def organization_exists(self, organization_id):
        return self.org_collection.count_documents({"id":organization_id})==1
    
    def delete_organization(self, organization_id):
        # remove membership first
        for user in self.list_organization_users(organization_id):
            self.delete_organization_membership(organization_id, user["id"])
        # remove the organization
        return self.org_collection.delete_one({"id":organization_id})

    def update_organization(self, organization):
        updated = self.__create_organization(organization)
#        new_value = {"$set": updated}
        self.org_collection.replace_one({"id":organization['id']}, updated)
        return updated
    
    def __create_organization(self, organization):
        print(organization)
        record = {
            'id': organization['id'],
            'name': organization['name'],
            'web': organization['web']
        }
        return record

    @staticmethod
    def load_connector():

        o = MongoDBUsersConnector()
        o.client = MongoClient("mongodb://"+os.environ['DB_HOST'])
        o.db = o.client[os.environ["DB_NAME"]]
        o.user_collection = o.db["users"]
        o.org_collection = o.db["organizations"]
        
        return o
