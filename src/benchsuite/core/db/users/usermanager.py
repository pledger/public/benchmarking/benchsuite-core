#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from .mongo import MongoDBUsersConnector

logger = logging.getLogger(__name__)


class UserManager:

    def __init__(self, **kwargs):
        self.dao = MongoDBUsersConnector(**kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return exc_type is None
        
    # organizations

    def list_organizations(self):
        return self.dao.list_organizations()

    def add_organization(self, organization):
        return self.dao.add_organization(organization)
    
    def get_organization(self, organization_id):
        return self.dao.get_organization(organization_id)
        
    def delete_organization(self, organization_id):
        return self.dao.delete_organization(organization_id)
        
    def update_organization(self, organization):
        return self.dao.update_organization(organization)
        
    def organization_exists(self, organization_id):
        return self.dao.organization_exists(organization_id)
        
    # users
        
    def list_users(self):
        return self.dao.list_users()
    
    def get_user(self, user_id):
        return self.dao.get_user(user_id)
        
    def list_organization_users(self, organization_id):
        return self.dao.list_organization_users(organization_id)
        
    def add_user(self, user):
        return self.dao.add_user(user)
        
    def update_user(self, user):
        return self.dao.update_user(user)
        
    def add_organization_membership(self, organization_id, user_id):
        return self.dao.add_organization_membership(organization_id, user_id)
        
    def delete_organization_membership(self, organization_id, user_id):
        return self.dao.delete_organization_membership(organization_id, user_id)
        
    def delete_user(self, user_id):
        return self.dao.delete_user(user_id)

    def user_exists(self, user_id):
        return self.dao.user_exists(user_id)

