#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json
import logging
from benchsuite.core.model.provider import load_provider_from_dict

from .mongo import MongoDBProvidersConnector
from ...configreader import BenchsuiteConfigParser

logger = logging.getLogger(__name__)


class ProviderManager:

    def __init__(self, **kwargs):
        self.dao = MongoDBProvidersConnector(**kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return exc_type is None
        
    # authz
    
    def _can_add_provider(self, user, provider):
        # any known user can add a provider
        return user!=None

    def _can_delete_provider(self, user, provider):
        # only the owner can delete a provider
        return "grants" in provider and "owner" in provider["grants"] and provider["grants"]["owner"] == "usr:"+user;

    def _can_update_provider(self, user, provider):
        # only the owner can update a provider
        return "grants" in provider and "owner" in provider["grants"] and provider["grants"]["owner"] == "usr:"+user;
    
    def _set_owner(self, user, provider):
        if user==None:
            return
        if provider==None:
            return
        if not "grants" in provider:
            provider["grants"] = {}
        provider["grants"]["owner"] = "usr:"+user
        # update 2020.10.10: providers are always public (with hidden metadata)
        provider["grants"]["viewer"] = "org:public"
        
    def __retainFields(self, object, fields):
        out = {}
        for f in fields:
            if f in object:
                out[f] = object[f]
        return out

    def hide_metadata(self, provider, requestorName=None):
        return self.__hideMetadata(provider, requestorName)

    def __hideMetadata(self, provider, requestorName=None):
        hide = False
        if requestorName==None:
#            if "grants" in provider and "owner" in provider["grants"]:
            hide = True
        else:
            if "grants" in provider and "owner" in provider["grants"]:
                if requestorName != provider["grants"]["owner"][4:]:
                    hide = True
        if hide:
            # keep only name, description and driver
            provider = self.__retainFields(provider, ["id", "name", "description", "driver", "grants"])
            del provider["grants"]["owner"]
#            del provider["secret_key"]
#            del provider["access_id"]
#            #del provider["driver"]
#            del provider["region"]
#            del provider["auth_url"]
#            provider["post_create_script"] = {}
#            provider["new_vm"] = {}
#            if "domain" in provider:
#                del provider["domain"]
#            if "tenant" in provider:
#                del provider["tenant"]
#            if "auth_version" in provider:
#                del provider["auth_version"]
            ## TODO: also remove other metadata
        return provider
        
    # providers ----------------------------

    def get_allowed_actions(self, provider_id, requestorName=None):
        if provider_id:
            provider = self.get_provider(provider_id, showsecrets=False, requestorName=requestorName);
            if provider==None:
                return {}
            elif requestorName==None:
                return {
                    "view": True
                }
            else:
                return {
                    "view": True,
                    "clone": True,
                    "edit": self._can_update_provider(requestorName, provider),
                    "delete": self._can_delete_provider(requestorName, provider),
                }
        else:
            if requestorName==None:
                return {}
            else:
                return {
                    "create": True
                }

    # add properties required by the scheduler and/or other services
    def OLD_enrich_for_compatibiliy(self, provider):
        if provider["driver"]=="openstack" or provider["driver"]=="ec2":
            provider["class"] = "benchsuite.stdlib.provider.libcloud.LibcloudComputeProvider"
        elif provider["driver"]=="vmware":
            provider["class"] = "benchsuite.stdlib.provider.vmware.VMWareProvider"
        elif provider["driver"] == "kubernetes":
            provider["class"] = "benchsuite.stdlib.provider.kubernetes.KubernetesProvider"
        return provider

    def list_provider_types(self):
        return []  # [OpenstackProvider(), KubernetesProvider()]
            
    def list_providers(self, showsecrets=False, requestorName=None, filters={}):
        providers = self.dao.list_providers(showsecrets, requestorName, filters=filters)
        out = []
        for p in providers:
            out.append(self.__hideMetadata(p, requestorName))
        return out

    def get_provider(self, provider_id, showsecrets=False, requestorName=None):
        provider = self.dao.get_provider(provider_id, showsecrets, requestorName)
        if provider:
            provider = self.__hideMetadata(provider, requestorName)
            return provider
        else:
            return None
         
    def add_provider(self, provider, showsecrets=False, requestorName=None):
        if(self._can_add_provider(requestorName, provider)):
            self._set_owner(requestorName, provider)
            #self._enrich_for_compatibiliy(provider)
            return self.dao.add_provider(provider, showsecrets, requestorName=requestorName)
        else:
            logger.error('requestor %s cannot add a provider', requestorName)
            return None
    
    def delete_provider(self, provider_id, requestorName=None):
        provider = self.get_provider(provider_id, requestorName = requestorName)
        if(self._can_delete_provider(requestorName, provider)):
            return self.dao.delete_provider(provider_id)
        else:
            return "unauthorized"

    def _is_primitive(self, thing):
        primitive = (int, str, bool)
        return isinstance(thing, primitive)

    
    def _patch(self, baseline, diff):
        for k in diff:
            # skip id
            if k=="id":
                continue
            newVal = diff[k]
            oldVal = None
            if k in baseline:
                oldVal = baseline[k]
            if newVal!=None and oldVal==None:
                # new property
                baseline[k] = newVal
            elif newVal==None and oldVal!=None:
                # removed property
                del baseline[k]
            elif newVal!=None and oldVal!=None:
                # modified value (might be a deep change)
                if type(newVal) is dict and type(oldVal) is dict:
                    baseline[k] = self._patch(oldVal, newVal)
                elif self._is_primitive(newVal) and self._is_primitive(oldVal):
                    baseline[k] = newVal;
                else:
                    logger.error("unsupported patching of " + str(oldVal) + " with " + str(newVal));
            else:
                # should not happen... removing a missing property
                logger.error("unforeseen condition: old value and diff are both None")
        return baseline
    
    def update_provider(self, provider, showsecrets=False, requestorName=None):
        existingProvider = self.get_provider(provider["id"], requestorName = requestorName)
        if not existingProvider:
            raise Exception('Cannot update provider with id=%s because it was not found in the db', provider['id'])
        if(self._can_update_provider(requestorName, existingProvider)):
            # TODO: should preserve previous permissions
            self._set_owner(requestorName, provider)
            #self._enrich_for_compatibiliy(provider)
            return self.dao.update_provider(provider, showsecrets)
        else:
            return "unauthorized"

    def patch_provider(self, providerDiff, showsecrets=False, requestorName=None):
        # patch the existing provider
        existingProvider = self.get_provider(providerDiff["id"], showsecrets=True, requestorName = requestorName)
        patchedProvider = self._patch(existingProvider, providerDiff)
        # update fully
        return self.update_provider(patchedProvider, showsecrets=showsecrets, requestorName=requestorName)
        
    def provider_exists(self, provider_id):
        return self.dao.provider_exists(provider_id)

    # images and flavours ------------------------------------
    
    # authz ok
    def list_images(self, provider_id, requestorName=None):
        # TODO: not tested
        provider = self.load_provider_from_db(provider_id, requestorName)
        if provider:
            return provider.list_images()
        else:
            return None
        
    # authz ok
    def list_sizes(self, provider_id, requestorName=None):
        # TODO: not tested
        provider = self.load_provider_from_db(provider_id, requestorName)
        if provider:
            return provider.list_sizes()
        else:
            return None

    def load_provider_from_db(self, provider_id, requestorName=None):
        provider = self.get_provider(provider_id, showsecrets=True, requestorName=requestorName)
        if provider:
            return load_provider_from_dict(provider)
        else:
            return None

    def OLD_to_config(self, provider):
        config = BenchsuiteConfigParser()

        sec = "provider"
        config.add_section(sec)
        
        for p in provider:
            if p == "post_create_script":
                continue
            if p == "service_properties":
                continue
            if p == 'metadata':
                config.set(sec, p, json.dumps(provider[p]))
                continue
            if p == "grants":
                continue
            if p == "new_vm":
                continue
            val = provider[p]
            if val!=None:
                config.set(sec, p, val)
        
        return config
