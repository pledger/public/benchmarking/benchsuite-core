#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
from benchsuite.core.db.mongo import MongoConnectorBase

logger = logging.getLogger(__name__)

KEY_NAMES_TO_ENCRYPT = ['private_key', 'secret', 'user_token', 'client_key', 'password', 'secret_key']


class MongoDBProvidersConnector(MongoConnectorBase):

    def __init__(self, db_host='localhost', db_port=27017, db_name='benchsuite', db_username=None, db_password=None, db_aes_key=None,
                 db_providers_collection='providers', **kwargs):
        super().__init__(db_host, db_port, db_name, db_username, db_password, db_aes_key, **kwargs)
        self.providers_collection = self.db[db_providers_collection]

    def __encrypt(self, msg):
        return self.crypto.encrypt(msg)
    
    def __decrypt(self, msg):
        return self.crypto.decrypt(msg)

    def add_provider(self, provider, showsecrets=False, requestorName=None):
        r = self.__encrypt_provider_rec(self.__create_provider(provider))
        self.providers_collection.insert_one(r)
        return self.get_provider(provider['id'], showsecrets, requestorName=requestorName)

    def get_provider(self, provider_id, showsecrets=False, requestorName=None):
        filter = self._getLoggedFilter(requestorName)
        filter["id"] = provider_id;
        p = self.providers_collection.find_one(filter, { "_id":0 })
        if p:
            return self.__decrypt_provider_rec(p, showsecrets)
        else:
            return None

    def update_provider(self, provider, showsecrets=False):
        updated = self.__encrypt_provider_rec(self.__create_provider(provider))
        self.providers_collection.replace_one({"id":provider['id']}, updated)
        return self.get_provider(provider['id'], showsecrets)

    def delete_provider(self, provider_id):
        return self.providers_collection.delete_one({"id":provider_id})
        
    def provider_exists(self, provider_id):
        return self.providers_collection.count_documents({"id":provider_id})==1
        
    # a filter to return only providers visible to the requestor
    def _getFilter(self, requestorName):
        filter = None
        if requestorName==None:
            filter= {"grants.viewer": "org:public"};
        else:
            filter = { "$or": [
                    {"grants.viewer": "org:public"},
                    {"grants.viewer": "usr:"+requestorName},
                    {"grants.owner": "usr:"+requestorName},
                ]
            }
        return filter

    # a filter to return only providers visible to a logged user
    def _getLoggedFilter(self, requestorName):
        filter = None
        if requestorName==None:
            filter= {"_id": "fakeid"};
        else:
            filter = {}
        return filter

    # a filter to return only providers owned by the requestor
    def _getOwnedFilter(self, requestorName):
        filter = None
        if requestorName==None:
            filter= {"_id": "fakeid"}
        else:
            filter = {"grants.owner": "usr:"+requestorName}
        return filter
        
    # return all providers owned by the requestor
    def list_providers(self, showsecrets=False, requestorName=None, filters={}):
        out = []
        if requestorName==None:
            return []
        else:
            filters.update(self._getOwnedFilter(requestorName))
            for provider in self.providers_collection.find(filters, { "_id":0 }):
                out.append(self.__decrypt_provider_rec(provider, showsecrets))
        return out

    def __create_provider(self, provider):
        record = {}
        for k in provider:
            record[k] = provider[k]
        return record

    def __encrypt_provider_rec(self, sub_dict):
        for k in sub_dict:
            if isinstance(sub_dict[k], dict):
                sub_dict[k] = self.__encrypt_provider_rec(sub_dict[k])
            else:
                if k in KEY_NAMES_TO_ENCRYPT and (sub_dict[k] is not None):
                    sub_dict[k] = self.__encrypt(sub_dict[k])

        return sub_dict

    def __decrypt_provider_rec(self, sub_dict, showsecrets = False):
        for k in sub_dict:
            if isinstance(sub_dict[k], dict):
                sub_dict[k] = self.__decrypt_provider_rec(sub_dict[k], showsecrets = showsecrets)
            else:
                if k in KEY_NAMES_TO_ENCRYPT and (sub_dict[k] is not None):
                    if showsecrets:
                        sub_dict[k] = self.__decrypt(sub_dict[k])
                    else:
                        sub_dict[k] = "********"

        return sub_dict
