#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import datetime
import logging

import os
from pymongo import MongoClient

from benchsuite.core.db.mongo import MongoConnectorBase

logger = logging.getLogger(__name__)


class MongoDBScheduleConnector(MongoConnectorBase):

    def __init__(self, db_host='localhost', db_port=27017, db_name='benchsuite', db_username=None, db_password=None, db_aes_key=None,
                 db_schedules_collection='scheduling', **kwargs):
        super().__init__(db_host, db_port, db_name, db_username, db_password, db_aes_key, **kwargs)
        self.schedules_collection = self.db[db_schedules_collection]

    def _getFilter(self, requestorName):
        filter = None
        if requestorName==None:
            filter = { "$or": [
                    {"grants.viewer": "org:public"},
                    {"grants.executor": "org:public"}
                ]
            }
        else:
            filter = { "$or": [
                    {"grants.viewer": "org:public"},
                    {"grants.viewer": "usr:"+requestorName},
                    {"grants.executor": "org:public"},
                    {"grants.executor": "usr:"+requestorName},
                    {"grants.owner": "usr:"+requestorName},
                ]
            }

        # hide 'hidden' schedules
        filter = { "$and": [
                filter,
                {"name": {"$not": {"$regex":"^_hidden"}}}
            ]}
        return filter

    # a filter to return only schedules owned by the requestor
    def _getOwnedFilter(self, requestorName):
        filter = None
        if requestorName==None:
            filter= {"_id": "fakeid"}
        else:
            filter = {"grants.owner": "usr:"+requestorName}
        
        # hide 'hidden' schedules
        filter = { "$and": [
                filter,
                {"name": {"$not": {"$regex":"^_hidden"}}}
            ]}
        return filter

    def add_schedule(self, schedule, requestorName=None):
        r = self.__create_schedule(schedule)
        self.schedules_collection.insert_one(r)
        return self.get_schedule(schedule['id'], requestorName)

    def get_schedule(self, schedule_id, requestorName=None):
        filter = self._getOwnedFilter(requestorName)
        filter["id"] = schedule_id;
        p = self.schedules_collection.find_one(filter, { "_id":0 })
        return p

    def update_schedule(self, schedule, requestorName=None):
        updated = self.__create_schedule(schedule)
        self.schedules_collection.replace_one({"id":schedule['id']}, updated)
        return self.get_schedule(schedule['id'], requestorName)

    def delete_schedule(self, schedule_id):
        return self.schedules_collection.delete_one({"id":schedule_id})
        
    def schedule_exists(self, schedule_id):
        return self.schedules_collection.count_documents({"id":schedule_id})==1
        
    def list_schedules(self, requestorName=None, filters={}):
        out = []
        filters.update(self._getOwnedFilter(requestorName))
        for schedule in self.schedules_collection.find(filters, {"_id":0}):
            out.append(schedule)
        return out

    def __create_schedule(self, schedule):
        record = {}
        for k in schedule:
            # Fix necessary because Flask marshalling does not work in some cases and leaves these fields as strings
            if (k == 'created_time' or k == 'updated_time') and isinstance(schedule[k], str):
                record[k] = datetime.datetime.strptime(schedule[k], '%Y-%m-%dT%H:%M:%S.%f')
                continue
            record[k] = schedule[k]

        return record

    @staticmethod
    def load_connector(db_host=None, db_port=27017, db_name=None, collection='scheduling'):
        if not db_host:
            db_host = os.environ.get('DB_HOST', "localhost")
        if not db_name:
            db_name = os.environ.get('DB_NAME', "benchsuite")
        o = MongoDBScheduleConnector()
        o.client = MongoClient(db_host, db_port)
        o.db = o.client[db_name]
        o.schedules_collection = o.db[collection]
        return o