#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import datetime
import logging
import configparser
import time

from .mongo import MongoDBScheduleConnector
from ..providers.providermanager import ProviderManager
from ..workloads.workloadmanager import WorkloadManager
from ...configreader import BenchsuiteConfigParser

logger = logging.getLogger(__name__)

class ScheduleManager:

    def __init__(self, provider_manager=None, workload_manager=None, **kwargs):
        self.dao = MongoDBScheduleConnector(**kwargs)
        self.pm = provider_manager or ProviderManager(**kwargs)
        self.wm = workload_manager or WorkloadManager(**kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return exc_type is None
        
    # Authorization stuff
    
    def _can_add_schedule(self, user, schedule):
        # any known user can add a schedule
        return user!=None
    
    def _can_delete_schedule(self, user, schedule):
        # hidden schedules cannot be deleted
        if schedule["name"].startswith("_hidden_"):
            return False
        # only the owner can delete a schedule
        return "grants" in schedule and "owner" in schedule["grants"] and schedule["grants"]["owner"] == "usr:"+user;

    def _can_update_schedule(self, user, schedule):
        # hidden schedules cannot be updated
        if schedule["name"].startswith("_hidden_"):
            return False
        # only the owner can update a schedule
        return "grants" in schedule and "owner" in schedule["grants"] and schedule["grants"]["owner"] == "usr:"+user;
    
    def _set_owner(self, user, schedule):
        if user==None:
            return
        if schedule==None:
            return
        if not "grants" in schedule:
            schedule["grants"] = {}
        schedule["grants"]["owner"] = "usr:"+user        

    def _set_executor(self, executor, schedule):
        if executor==None:
            return
        if schedule==None:
            return
        if not "grants" in schedule:
            schedule["grants"] = {}
        schedule["grants"]["executor"] = "usr:"+executor        

    def _make_private(self, schedule):
        if not "grants" in schedule:
            schedule["grants"] = {}
        if "viewer" in schedule["grants"]:
            del schedule["grants"]["viewer"]

    def _fix_reports_scope(self, requestor, schedule):
        if requestor==None:
            return
        if schedule==None:
            return
        if not "reports-scope" in schedule:
            schedule["reports-scope"] = "usr:"+requestor
        elif schedule["reports-scope"] == "usr:me":
            schedule["reports-scope"] = "usr:"+requestor

    # hide metadata, depending on the requestor grants
    def __hideMetadata(self, schedule, requestorName=None, requestorOrg=None):

        # the owner of this scehdule
        owner = None
        if "owner" in schedule["grants"]:
            owner = schedule["grants"]["owner"][4:]
        
        # the organization of the requestor
        if requestorName=="gabriele":
            requestorOrg = "eng"
        elif requestorName=="paolo":
            requestorOrg = "eng"
        else:
            requestorOrg = "acme"
        
        # show only relevant grants
        hide = False
        if requestorName==None:
            hide = True
        else:
            if "grants" in schedule and "owner" in schedule["grants"]:
                if requestorName != schedule["grants"]["owner"][4:]:
                    del schedule["grants"]["owner"]
            if "grants" in schedule and "executor" in schedule["grants"]:
                if schedule["grants"]["executor"]!="org:public":
                    if schedule["grants"]["executor"]!= ("usr:"+requestorName) and schedule["grants"]["executor"]!= ("org:"+requestorOrg): 
                        del schedule["grants"]["executor"]

#        if hide:
#            if "owner" in schedule["grants"]:
#                del schedule["grants"]["owner"]
#            if "executor" in schedule["grants"]:
#                del schedule["grants"]["executor"]
            ## TODO: also remove other metadata, where applicable
            
            
        # show only relevant scopes
        if "reports-scope" in schedule:
            scope = schedule["reports-scope"]
            
            # anonymous will get public or hidden
            if requestorName==None and scope!="org:public":
                del schedule["reports-scope"]
                
            # user scope, but different user
            elif scope[:4] == "usr:": 
                user = scope[4:]
                if requestorName==None or requestorName!=user:
                    del schedule["reports-scope"]

            # org scope, but different org 
            elif scope[:4] == "org:" and scope!="org:public" and requestorName!=owner and requestorOrg!=scope[4:]: 
                org = scope[4:]
                if requestorOrg==None or requestorOrg!=org:
                    del schedule["reports-scope"]

        ## TODO: also remove other metadata, where applicable
            
        return schedule

    # add properties required by the scheduler and/or other services
    def _enrich_for_compatibiliy(self, schedule, requestorName=None):
        schedule["username"] = str(requestorName)
        schedule["provider_config_secret"] = ""
        return schedule
    
    def _set_create_time(self, schedule):
        schedule["created_time"] = datetime.datetime.now()

    def _set_update_time(self, schedule):
        schedule["updated_time"] = datetime.datetime.now()

    def get_allowed_actions(self, schedule_id, requestorName=None):
        if schedule_id:
            schedule = self.get_schedule(schedule_id, requestorName=requestorName);
            if schedule==None:
                return {}
            elif requestorName==None:
                return {
                    "view": True
                }
            else:
                return {
                    "view": not schedule["name"].startswith("_hidden_"),
                    "clone": not schedule["name"].startswith("_hidden_"),
                    "edit": self._can_update_schedule(requestorName, schedule),
                    "delete": self._can_delete_schedule(requestorName, schedule),
                }
        else:
            if requestorName==None:
                return {}
            else:
                return {
                    "create": True
                }            
        
    # schedules ----------------------------

    def list_schedules(self, requestorName=None, embedProvider=False, embedWorkloads=False, filters={}):
        schedules = self.dao.list_schedules(requestorName, filters=filters)
        out = []
        for s in schedules:
            if embedProvider or embedWorkloads:
                news = self.get_schedule(s["id"], requestorName=requestorName, embedProvider=embedProvider, embedWorkloads=embedWorkloads)
                out.append(self.__hideMetadata(news, requestorName))
            else:
                out.append(self.__hideMetadata(s, requestorName))
        return out

    def add_schedule(self, schedule, requestorName=None):
        if(self._can_add_schedule(requestorName, schedule)):
            self._set_owner(requestorName, schedule)
            self._set_executor(requestorName, schedule)
            self._fix_reports_scope(requestorName, schedule)
            self._make_private(schedule)
            self._enrich_for_compatibiliy(schedule, requestorName=requestorName)
            self._set_create_time(schedule)
            return self.dao.add_schedule(schedule, requestorName=requestorName)
        else:
            logger.error('requestor %s cannot add a schedule', requestorName)
            return None    

    def delete_schedule(self, schedule_id, requestorName=None):
        schedule = self.get_schedule(schedule_id, requestorName = requestorName)
        if(self._can_delete_schedule(requestorName, schedule)):
            return self.dao.delete_schedule(schedule_id)
        else:
            return "unauthorized"        
        
    def update_schedule(self, schedule, showsecrets=False, requestorName=None):
        existingSchedule = self.get_schedule(schedule["id"], requestorName = requestorName)
        if(self._can_update_schedule(requestorName, existingSchedule)):
            self._set_owner(requestorName, schedule)
            self._set_executor(requestorName, schedule)
            self._make_private(schedule)
            self._fix_reports_scope(requestorName, schedule)
            self._enrich_for_compatibiliy(schedule, requestorName=requestorName)
            self._set_update_time(schedule)
            return self.dao.update_schedule(schedule)
        else:
            return "unauthorized"
        
    def schedule_exists(self, schedule_id):
        return self.dao.schedule_exists(schedule_id)

    def get_schedule(self, schedule_id, requestorName=None, embedWorkloads=False, embedProvider=False):
        schedule = self.dao.get_schedule(schedule_id, requestorName)
        if schedule:
            schedule = self.__hideMetadata(schedule, requestorName)
            # embed provider
            if embedProvider:
                if "provider_id" in schedule:
                    schedule["provider"] = self.pm.get_provider(schedule["provider_id"], False, requestorName=requestorName)
            else:
                if "provider" in schedule:
                    del schedule["provider"]
            # embed workloads
            if embedWorkloads:
                schedule["workloads"] = []
                if "tests" in schedule:
                    for t in schedule["tests"]:
                        workload = self.wm.get_workload(t, requestorName=requestorName)
                        schedule["workloads"].append(workload)
            else:
                if "workloads" in schedule:
                    del schedule["workloads"]
        return schedule

    def to_config(self, wl):
        config = BenchsuiteConfigParser()

        sec = wl["schedule_name"]
        config.add_section(sec)
        
        config.set(sec, "schedule_name", sec)
        
        # generic fields
        for p in wl:
            print(p)
            if p == "grants":
                continue
            if p=="categories" or p=="schedule_parameters":
                continue
            val = wl[p]
            print(val)
            if val!=None:
                config.set(sec, p, val)
        
        if "categories" in wl:
            val = ", ".join(wl["categories"])
            config.set(sec, "categories", val)
        
        # custom fields
        for k in wl["schedule_parameters"]:
            val = wl["schedule_parameters"][k]
            if val!=None:
                config.set(sec, k, val)
        
        return config
