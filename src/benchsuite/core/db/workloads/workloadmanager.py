#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import configparser
import uuid

from .mongo import MongoDBWorkloadConnector

logger = logging.getLogger(__name__)


class WorkloadManager:

    def __init__(self, **kwargs):
        self.dao = MongoDBWorkloadConnector(**kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return exc_type is None
        
    # authz
    
    def _can_add_workload(self, user, workload):
        # any known user can add a workload
        return user!=None

    def _can_delete_workload(self, user, workload):
        # only the owner can delete a workload
        return "grants" in workload and "owner" in workload["grants"] and workload["grants"]["owner"] == "usr:"+user;

    def _can_update_workload(self, user, workload):
        # only the owner can update a workload
        return "grants" in workload and "owner" in workload["grants"] and workload["grants"]["owner"] == "usr:"+user;
    
    # add properties required by the scheduler and/or other services
    def _enrich_for_compatibiliy(self, workload):

        if 'image' in workload['workload_parameters']:
            workload["class"] = "benchsuite.stdlib.benchmark.docker_benchmark.DockerBenchmark"
        else:
            workload["class"] = "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark"

        ltn = workload["tool_name"].lower()
        parser = "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
        if ltn == "dacapo":
            parser = "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
        elif ltn == "filebench":
            parser = "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
        elif ltn == "iperf":
            parser = "benchsuite.stdlib.benchmark.parsers.IPerfResultParser"
        elif ltn == "sysbench":
            parser = "benchsuite.stdlib.benchmark.parsers.SysbenchResultParser"
        elif ltn == "web framework benchmarking":
            parser = "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
        elif ltn == "ycsb mongodb":
            parser = "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
        elif ltn == "ycsb mysql":
            parser = "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
        workload["parser"] = parser
        return workload
    
    def _set_owner(self, user, workload):
        if user==None:
            return
        if workload==None:
            return
        if not "grants" in workload:
            workload["grants"] = {}
        workload["grants"]["owner"] = "usr:"+user        

    def _set_executor(self, executor, workload):
        if executor==None:
            return
        if workload==None:
            return
        if not "grants" in workload:
            workload["grants"] = {}
        workload["grants"]["executor"] = "org:public"      

    # hide metadata, depending on the requestor grants
    def __hideMetadata(self, workload, requestorName=None, export=False):
        hide = False
        if requestorName==None:
            if "grants" in workload and "owner" in workload["grants"]:
                hide = True
        else:
            if "grants" in workload and "owner" in workload["grants"]:
                if requestorName != workload["grants"]["owner"][4:]:
                    hide = True
        if hide:
            del workload["grants"]["owner"]
            
        if export:
            del workload["grants"]
        ## TODO: also remove other metadata, where applicable
        return workload


    def get_allowed_actions(self, workload_id, requestorName=None):
        if workload_id:
            workload = self.get_workload(workload_id,requestorName=requestorName);
            if workload==None:
                return {}
            elif requestorName==None:
                return {
                    "view": True
                }
            else:
                return {
                    "view": True,
                    "clone": True,
                    "edit": self._can_update_workload(requestorName, workload),
                    "delete": self._can_delete_workload(requestorName, workload),
                }
        else:
            if requestorName==None:
                return {}
            else:
                return {
                    "create": True
                }

    def list_workloads(self, filter={}, requestorName=None, embed_parent=False, resolve=False, export=False):
        workloads = self.dao.list_workloads(filter, requestorName)
        out = []
        for w in workloads:
            workload = self.__hideMetadata(w, requestorName, export)
            # in export mode, skip abstract workloads when resolving properties
            if resolve and export and "abstract" in workload and workload["abstract"]==True:
                continue
            out.append(workload)
            if (embed_parent or resolve) and "parent_workload_id" in workload:
                parent_workload = self.get_workload(workload["parent_workload_id"], embed_parent=embed_parent, resolve=resolve)
                if embed_parent:
                    workload["parent_workload"] = parent_workload
                if resolve:
                    workload = self._combine_workloads(parent_workload, workload)
                    if export:
                        del workload["parent_workload_id"]
        if export:
            out = self._replace_ids(out, {})
        return out
    
    def add_workload(self, workload, requestorName=None):
        if(self._can_add_workload(requestorName, workload)):
            self._set_owner(requestorName, workload)
#            self._set_executor("org:public", workload)
            self._enrich_for_compatibiliy(workload)
            return self.dao.add_workload(workload, requestorName)
        else:
            return None
    
    def delete_workload(self, workload_id, requestorName=None):
        workload = self.get_workload(workload_id, requestorName = requestorName)
        if(self._can_delete_workload(requestorName, workload)):
            return self.dao.delete_workload(workload_id)
        else:
            return "unauthorized"
        
    def update_workload(self, workload, requestorName=None):
        existingWorkload = self.get_workload(workload["id"], requestorName = requestorName)
        if(self._can_update_workload(requestorName, existingWorkload)):
            # TODO: should preserve previous permissions
            self._set_owner(requestorName, workload)
#            self._set_executor("org:public", workload)
            self._enrich_for_compatibiliy(workload)
            if "parent_workload" in workload:
                del workload["parent_workload"]
            if "children_workloads" in workload:
                del workload["children_workloads"]
            return self.dao.update_workload(workload, requestorName)
        else:
            return "unauthorized"        
        
        
    def workload_exists(self, workload_id):
        return self.dao.workload_exists(workload_id)

    def _replace_ids(self, workloads, dictionary):
        # an array of workloads
        if isinstance(workloads, list):
            out = []
            for w in workloads:
                out.append(self._replace_ids(w, dictionary))
            return out
        # a single workload
        else:
            w = workloads
            # replace the workload id
            id = w["id"]
            if not id in dictionary:
                dictionary[id] = str(uuid.uuid4())
            w["id"] = dictionary[id]
            # replace the parent id
            if "parent_workload_id" in w:
                id = w["parent_workload_id"]
                if not id in dictionary:
                    dictionary[id] = str(uuid.uuid4())
                w["parent_workload_id"] = dictionary[id]
            # now process the parent
            if "parent_workload" in w:
                w["parent_workload"] = self._replace_ids(w["parent_workload"], dictionary)
            if "children_workloads" in w:
                w["children_workloads"] = self._replace_ids(w["children_workloads"], dictionary)
            return w

    def get_workload(self, workload_id, requestorName=None, embed_parent=False, embed_children=False, resolve=False, export=False):
        workload = self.dao.get_workload(workload_id, requestorName=requestorName)
        if workload:
            if (embed_parent or resolve) and "parent_workload_id" in workload:
                parent_workload = self.get_workload(workload["parent_workload_id"], requestorName=requestorName, embed_parent=embed_parent, resolve=resolve)
                if embed_parent:
                    workload["parent_workload"] = parent_workload
                if resolve:
                    workload = self._combine_workloads(parent_workload, workload)
                    del workload["parent_workload_id"]
            if (embed_children):
                workload["children_workloads"] = []
                children = self.dao.get_workloads_by_parent_id(parent_workload_id=workload_id, requestorName=requestorName)
                for c in children:
                    c = self.__hideMetadata(c, requestorName, export)
                    workload["children_workloads"].append(c)

            workload = self.__hideMetadata(workload, requestorName, export)
            if export:
                workload = self._replace_ids(workload, dictionary={})
            return workload
        else:
            return None

    def get_workload_hierarchy_for_export(self, workload_id, requestorName=None):
        out = self.get_workload_hierarchy(workload_id, requestorName)
        for w in out:
            self.__hideMetadata(w, requestorName, True)
        return self._replace_ids(out, dictionary={})

    def get_workload_hierarchy(self, workload_id, requestorName=None):
        out = []
        workload = self.dao.get_workload(workload_id, requestorName=requestorName)
        if workload:
            out.append(workload)
            if "parent_workload_id" in workload:
                ancestors = self.get_workload_hierarchy(workload["parent_workload_id"], requestorName=requestorName)
                out = out+ancestors
        return out;

    def _combine_workloads(self, parent_workload, workload):
        if parent_workload == None:
            return workload
        # overwrite properties
        for k in parent_workload:
            if k in ["abstract", "grants", "id", "parent_workload_id", "categories"]:
                continue
            if not k in workload:
                workload[k] = parent_workload[k]
        # workload parameters
        if not "workload_parameters" in workload:
            workload["workload_parameters"] = {}
        if not "workload_parameters" in parent_workload:
            parent_workload["workload_parameters"] = {}
        workload["workload_parameters"] = {**parent_workload["workload_parameters"], **workload["workload_parameters"]}
        # concatenate categories
        if not "categories" in workload:
            workload["categories"] = []
        if "categories" in parent_workload:
            workload["categories"] +=  parent_workload["categories"]
        return workload

    def get_workloads_by_name(self, tool_name, workload_name, requestorName=None):
        workload = self.dao.get_workloads_by_name(tool_name, workload_name, requestorName=requestorName)
        if workload:
            workload = self.__hideMetadata(workload, requestorName)
            return workload
        else:
            return None
    
    def to_config(self, wl):
        config = BenchsuiteConfigParser()

        sec = wl["workload_name"]
        config.add_section(sec)
        
        config.set(sec, "workload_name", sec)
        
        # generic fields
        for p in wl:
            if p == "grants":
                continue
            if p=="categories" or p=="workload_parameters":
                continue
            val = wl[p]
            if val!=None:
                config.set(sec, p, val)
        
        if "categories" in wl:
            val = ", ".join(wl["categories"])
            config.set(sec, "categories", val)
        
        # custom fields
        for k in wl["workload_parameters"]:
            val = wl["workload_parameters"][k]
            if val!=None:
                config.set(sec, k, val)
        
        return config
