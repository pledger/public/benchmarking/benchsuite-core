#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

import os
from pymongo import MongoClient

from benchsuite.core.db.mongo import MongoConnectorBase

logger = logging.getLogger(__name__)


class MongoDBWorkloadConnector(MongoConnectorBase):

    def __init__(self, db_host='localhost', db_port=27017, db_name='benchsuite', db_username=None, db_password=None, db_aes_key=None,
                 db_workloads_collection='workloads', **kwargs):
        super().__init__(db_host, db_port, db_name, db_username, db_password, db_aes_key, **kwargs)
        self.workloads_collection = self.db[db_workloads_collection]

    # a filter to return only workloads visible to the requestor
    def _getFilter(self, requestorName):
        filter = None
        if requestorName==None:
            filter= {"grants.executor": "org:public"};
        else:
            filter = { "$or": [
                    {"grants.executor": "org:public"},
                    {"grants.executor": "usr:"+requestorName},
                    {"grants.owner": "usr:"+requestorName},
                ]
            }
        return filter

    def add_workload(self, workload, requestorName=None):
        r = self.__create_workload(workload)
        self.workloads_collection.insert_one(r)
        return self.get_workload(workload['id'], requestorName)

    def get_workload(self, workload_id, requestorName=None):
        filter = self._getFilter(requestorName)
        filter["id"] = workload_id;
        out =  self.workloads_collection.find_one(filter, { "_id":0 })
        return out

    def get_workloads_by_name(self, tool_name, workload_name, requestorName=None):
        filter = self._getFilter(requestorName)
        if tool_name != None and tool_name != "":
            filter["tool_name"] = tool_name
        if workload_name != None and tool_name != "":
            filter["workload_name"] = workload_name
        workloads = self.workloads_collection.find(filter, { "_id":0 })
        out = []
        for w in workloads:
            print("appending workload " + (str(w)))
            out.append(w)
        return out

    def get_workloads_by_parent_id(self, parent_workload_id=id, requestorName=None):
        filter = self._getFilter(requestorName)
        if parent_workload_id != None and parent_workload_id != "":
            filter["parent_workload_id"] = parent_workload_id
        workloads = self.workloads_collection.find(filter, { "_id":0 })
        out = []
        for w in workloads:
            out.append(w)
        return out

    def update_workload(self, workload, requestorName=None):
        updated = self.__create_workload(workload)
        self.workloads_collection.replace_one({"id":workload['id']}, updated)
        return self.get_workload(workload['id'], requestorName)

    def delete_workload(self, workload_id):
        return self.workloads_collection.delete_one({"id":workload_id})
        
    def workload_exists(self, workload_id):
        return self.workloads_collection.count_documents({"id":workload_id})==1
        
    def list_workloads(self, filter={}, requestorName=None):
        # build a filter
        _filter = self._getFilter(requestorName)

        if filter.get('tool', None):
            _filter["tool"] = filter["tool"]
        if filter.get('workload', None):
            _filter["workload"] = filter["workload"]

        # retrieve workloads
        out = []
        for workload in self.workloads_collection.find(_filter, {'_id': 0}):
            out.append(workload)
        return out        

    def __create_workload(self, workload):
        record = {}
        for k in workload:
            record[k] = workload[k]
        return record
