#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import copy

from datetime import time, date
from uuid import UUID
from bson import ObjectId
from pydantic import BaseModel

from attrdict import AttrDict

from benchsuite.core.db.providers.providermanager import ProviderManager
from benchsuite.core.db.schedules.schedulemanager import ScheduleManager
from benchsuite.core.db.workloads.workloadmanager import WorkloadManager
from benchsuite.core.db.executions.executionmanager import ExecutionManager
from benchsuite.core.model.benchmark import load_workload_from_dict
from benchsuite.core.model.schedule import Schedule

logger = logging.getLogger(__name__)


def provider_to_dict(prov):
    """
    assuming it is a Pydantic model
    :param prov:
    :return:
    """
    return prov.dict()


def dict_to_schedule(provdict):
    return Schedule.parse_obj(provdict)
    for k, v in provdict.items():
        setattr(instance, k, v)
    return instance


# inspired by : https://github.com/tiangolo/fastapi/issues/1515
def mongo_json_encoder(record: [dict, list, BaseModel]):
    def convert_type(data):
        if isinstance(data, time):
            return str(data)
        elif isinstance(data, (UUID, ObjectId)):
            return str(data)
        elif isinstance(data, list):
            return list(map(convert_type, data))
        elif isinstance(data, dict):
            return mongo_json_encoder(data)
        try:
            return data
        except TypeError as ex:
            raise TypeError({"error_msg": str(ex), "key": key, "value": value, "type": type(value)})

    if isinstance(record, BaseModel):
        return mongo_json_encoder(record.dict(by_alias=True))
    elif isinstance(record, dict):
        for key, value in record.items():
            record[key] = convert_type(value)
        return record
    else:
        return list(map(mongo_json_encoder, record))


class BenchsuiteConfigurationDB:

    def __init__(self, return_attrdict_objects=True, **kwargs):
        self.attrdict_enabled = return_attrdict_objects

        self.pm = ProviderManager(**kwargs)
        self.wm = WorkloadManager(**kwargs)
        self.sm = ScheduleManager(provider_manager=self.pm, workload_manager=self.wm, **kwargs)
        self.em = ExecutionManager(provider_manager=self.pm, **kwargs)

        self.requestor = kwargs.get('db_requestor', None)

    def as_user(self, requestor):
        new_obj = copy.copy(self)
        new_obj.requestor = requestor
        return new_obj

    def list_providers(self, filters={}, show_certs=False):
        return [self.__fmt_return(x) for x in self.pm.list_providers(requestorName=self.requestor, filters=filters, showsecrets=show_certs)]

    def update_provider(self, provider):
        self.pm.update_provider(provider_to_dict(provider), requestorName=self.requestor)

    def add_provider(self, provider):
        self.pm.add_provider(provider_to_dict(provider), requestorName=self.requestor)

    def get_provider(self, provider_id, show_secrets=False):
        return self.__fmt_return(self.pm.get_provider(provider_id, requestorName=self.requestor, showsecrets=show_secrets))

    def delete_provider(self, provider):
        self.pm.delete_provider(provider.id, requestorName=self.requestor)

    def list_workloads(self):
        return [load_workload_from_dict(x) for x in self.wm.list_workloads(requestorName=self.requestor)]

    def get_workload(self, workload_id, resolve=False):
        return load_workload_from_dict(self.wm.get_workload(workload_id, requestorName=self.requestor, resolve=resolve))

    def delete_workload(self, workload_or_id):
        self.wm.delete_workload(workload_or_id.id if isinstance(workload_or_id, BaseModel) else workload_or_id, requestorName=self.requestor)

    def list_schedules(self, filters={}):
        return [Schedule.parse_obj(x) for x in self.sm.list_schedules(requestorName=self.requestor, filters=filters)]

    def get_schedule(self, schedule_id, embed_workloads=False, embed_provider=False) -> Schedule:
        return Schedule.parse_obj((self.sm.get_schedule(schedule_id, requestorName=self.requestor, embedWorkloads=embed_workloads, embedProvider=embed_provider)))

    def update_schedule(self, schedule):
        self.sm.update_schedule(mongo_json_encoder(schedule), requestorName=self.requestor)

    def add_schedule(self, schedule):
        self.sm.add_schedule(mongo_json_encoder(schedule), requestorName=self.requestor)

    def delete_schedule(self, schedule):
        self.sm.delete_schedule(schedule.id, requestorName=self.requestor)

    def list_executions(self):
        return self.em.list_executions(requestorName=self.requestor)

    def get_execution(self, execution_id):
        return self.em.get_execution(execution_id, requestorName=self.requestor)

    def update_execution(self, execution):
        self.em.update_execution(mongo_json_encoder(execution), requestorName=self.requestor)

    def add_execution(self, execution):
        self.em.add_execution(mongo_json_encoder(execution), requestorName=self.requestor)

    def __fmt_return(self, obj):
        if self.attrdict_enabled:
            if obj is None:
                return None
            return AttrDict(obj)
        else:
            return obj
