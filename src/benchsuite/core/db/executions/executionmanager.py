#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from .mongo import MongoDBExecutionConnector
from ..providers.providermanager import ProviderManager

logger = logging.getLogger(__name__)


class ExecutionManager:

    def __init__(self, provider_manager=None, **kwargs):
        self.dao = MongoDBExecutionConnector(**kwargs)
        self.pm = provider_manager or ProviderManager(**kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return exc_type is None

    def _hide_metadata(self, execution, requestorName=None, requestorOrg=None):

        reports_scope = "xxx:nobody"
        executor = None

        if "schedule" in execution:
            schedule = execution["schedule"]
            # record the reports_scope
            if "reports-scope" in schedule:
                reports_scope = schedule["reports-scope"]
            # record the executor
            if "properties" in schedule and "executor" in schedule["properties"]:
                executor = schedule["properties"]["executor"]
            # mark the execution as one-time, if applicable
            if schedule["name"].startswith("_hidden_schedule"):
                execution["one-time"] = True
        
        # keep the schedule only if requestor == schedule.owner 
        if "schedule" in execution:
            schedule = execution["schedule"]
            if requestorName!=None:
                if "grants" in schedule and "owner" in schedule["grants"]:
                    schedule_owner = schedule["grants"]["owner"][4:]
                    if requestorName==schedule_owner:
                        # keeping the schedule
                        {}
                    else:
                        del execution["schedule"]
                else:
                    del execution["schedule"]
            else:
                del execution["schedule"]
                
        # keep the executor only if requestor == executor
        if requestorName!=None and requestorName==executor:
            execution["executor"] = executor

        # provider
        if execution.get('provider', None):
            provider = execution["provider"]

            provider_owner = None
            if "grants" in provider and "owner" in provider["grants"]:
                provider_owner = provider["grants"]["owner"][4:]

            if requestorName==None:
                ## OK requestorname = none => hide completely
                del execution["provider"]
            elif (requestorOrg and reports_scope=="org:"+requestorOrg) or (requestorName and reports_scope=="usr:"+requestorName) or (requestorName and reports_scope=="org:public"):
                ## requestor in reports_scope => show basic info
                execution["provider"] = provider = self.pm.hide_metadata(execution["provider"], requestorName=requestorName)
            elif requestorName==provider_owner:
                ## requestor is provider.owner => show fully 
                # keep the full provider
                {}
            else:
                del execution["provider"]

        return execution        
        
    def list_executions(self, requestorName=None, requestorOrg=None):
        executions = self.dao.list_executions(requestorName=requestorName, requestorOrg=requestorOrg)
        for e in executions:
            self._hide_metadata(e, requestorName=requestorName, requestorOrg=requestorOrg)
#        for e in executions:
#            self._enrich_with_schedule_and_provider(e, requestorName=requestorName)
        return executions

    def execution_exists(self, execution_id, requestorName=None):
        return self.dao.execution_exists(execution_id, requestorName=requestorName)

    def add_execution(self, execution, requestorName=None):
        return self.dao.add_execution(execution)

    def update_execution(self, execution, requestorName=None):
        # TODO check if the requestor can update the execution
        return self.dao.update_execution(execution)

    def get_execution(self, execution_id, requestorName=None, requestorOrg=None):
        execution = self.dao.get_execution(execution_id, requestorName=requestorName, requestorOrg=requestorOrg)
        if execution:
            self._hide_metadata(execution, requestorName=requestorName, requestorOrg=requestorOrg)
#        return self._enrich_with_schedule_and_provider(execution, requestorName=requestorName)
        return execution

    # return the request (schedule or one-time-schedule) that triggered the execution
    def get_request_for_execution(self, execution_id, requestorName=None, requestorOrg=None):
        execution = self.dao.get_execution(execution_id, requestorName=requestorName, requestorOrg=requestorOrg)
        if execution:
            if "schedule" in execution:
                schedule = execution["schedule"]
                # Only executions triggered by me can be returned (i.e. owner is me)
                if "grants" in schedule:
                    if "owner" in schedule["grants"]:
                        if "usr:"+requestorName == schedule["grants"]["owner"]:
                            # Make it a one-time request
                            schedule["name"] = '_hidden_schedule'
                            schedule["id"] = None
                            schedule["grants"] = {"owner":requestorName}
                            if not "hidden" in schedule["tags"]:
                                schedule["tags"].append("hidden")
                            schedule["active"] = True
                            schedule["interval"] = {'weeks':500}
#                            del schedule["reports-scope"]
                            del schedule["username"]
                            del schedule["provider_config_secret"]
                            del schedule["properties"]["executor"]
                            return execution["schedule"]
        return None
    
    def list_runs(self, execution_id, requestorName=None, requestorOrg=None):
        return self.dao.list_runs(execution_id, requestorName=requestorName, requestorOrg=requestorOrg, hide=True)
    
    def get_allowed_actions(self, execution_id, requestorName=None):
        if execution_id:
            execution = self.get_execution(execution_id, requestorName=requestorName);
            if provider==None:
                return {}
            elif requestorName==None:
                return {
                    "view": True
                }
            else:
                return {
                    "view": True
                }
        else:
            if requestorName==None:
                return {}
            else:
                return {
                    "create": True
                }
                
    def _enrich_with_schedule_and_provider(self, execution, requestorName=None):
        with ScheduleManager() as sm:
            with ProviderManager() as pm:
                if execution and "schedule_id" in execution:
                    schedule = sm.get_schedule(execution["schedule_id"], requestorName=requestorName, embedProvider=True, embedWorkloads=True)
                    if schedule:
                        execution["schedule"] = schedule
                        provider = pm.get_provider(schedule["provider_id"], False, requestorName=requestorName)
                        if provider:
                            execution["provider"] = provider
        return execution
                