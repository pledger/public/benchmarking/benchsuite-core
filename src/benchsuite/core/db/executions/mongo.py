#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

import os
from pymongo import MongoClient
from pymongo import DESCENDING

from benchsuite.core.db.mongo import MongoConnectorBase

logger = logging.getLogger(__name__)


class MongoDBExecutionConnector(MongoConnectorBase):

    def __init__(self, db_host='localhost', db_port=27017, db_name='benchsuite', db_username=None, db_password=None, db_aes_key=None,
                 db_executions_collection='executions', db_results_collection='results', db_errors_collection='exec_errors', **kwargs):
        super().__init__(db_host, db_port, db_name, db_username, db_password, db_aes_key, **kwargs)
        self.executions_collection = self.db[db_executions_collection]
        self.results_collection = self.db[db_results_collection]
        self.errors_collection = self.db[db_errors_collection]

    # a filter on executions

    # 1. owner dello schedule. 
    # 2. scope is me 
    # 3. scope is my org 
    # 4. scope is public. Else hide
    
    def _getExecutionsFilter(self, requestorName=None, requestorOrg=None):
        filter = None
        if requestorName==None:
            filter= {"_id": "fakeid"}
        else:
            filter = { "$or": [
                    {"schedule.properties.executor": str(requestorName)},
                    {"schedule.reports-scope": "usr:"+str(requestorName)},
                    # FIXME: requestorOrg could be none. If so, do not include the following check
                    {"schedule.reports-scope": "org:"+str(requestorOrg)},
                    {"schedule.reports-scope": "org:public"},
                ]
            }
            filter = { "$and": [
                    filter,
                    {"starttime":{ "$exists": True}}
                ]
            }
        return filter

    def _getExecutionFilter(self, execution_id, requestorName=None, requestorOrg=None):
        filter = None
        if requestorName==None:
            filter= {"_id": "fakeid"}
        else:
            filter = { "$or": [
                    {"schedule.properties.executor": str(requestorName)},
                    {"schedule.reports-scope": "usr:"+str(requestorName)},
                    # FIXME: requestorOrg could be none. If so, do not include the following check
                    {"schedule.reports-scope": "org:"+str(requestorOrg)},
                    {"schedule.reports-scope": "org:public"},
                ]
            }
            filter = { "$and": [
                    filter,
                    {"id":execution_id}
                ]
            }
        return filter
    
    def __enrich_with_inner_results(self, execution, requestorName=None, requestorOrg=None):

        # if already has this info, skip
        if "runs_status" in execution:
            return execution

        # only if completed        
        if execution["status"] != "RUNNING":
            runs_status = {"ok": [], "error": []}
            runs = self.list_runs(execution["id"], requestorName=requestorName, requestorOrg=requestorOrg)
            for r in runs:
                has_errors = False
                for k, v in r['status']['steps'].items():
                    if v['errno'] != 0:
                        has_errors = True
                        break

                if not has_errors:
                    runs_status["ok"].append(r["_id"])
                else:
                    runs_status["error"].append(r["_id"])

            filter = {"id":execution["id"]}
            update = {"$set": {"runs_status":runs_status}}
            execution["runs_status"] = runs_status
            logger.debug('Setting runs_status for execution %s: %s', execution['id'], runs_status)
            logger.debug(self.executions_collection)
            self.executions_collection.update(filter, update)

        return execution

    def _get_run_fields(self, execution, requestorName=None):
        executor = None
        if "schedule" in execution:
            if "properties" in execution["schedule"]:
                if "executor" in execution["schedule"]["properties"]:
                    executor = execution["schedule"]["properties"]["executor"]

        if requestorName==None:
            # nothing to anonymous users
            return {}
        if requestorName==executor:
            # everything to executor
            return None
        else:
            # just few fields to authenticated, non executor
            # not logs
            return {"metrics":1, "starttime":1, "test":1, "provider":1, "execution.environment":1}

    def _get_run_error_fields(self, execution, requestorName=None):
        executor = None
        if "schedule" in execution:
            if "properties" in execution["schedule"]:
                if "executor" in execution["schedule"]["properties"]:
                    executor = execution["schedule"]["properties"]["executor"]

        if requestorName==None:
            # nothing to anonymous users
            return {}
        if requestorName==executor:
            # everything to executor
            return None
        else:
            # just few fields to authenticated, non executor
            # not logs
            return {"tool":1, "workload":1, "test":1, "provider":1, "timestamp":1, "environment":1}

    def list_executions(self, requestorName=None, requestorOrg=None):
        out = []
        filter = self._getExecutionsFilter(requestorName=requestorName, requestorOrg=requestorOrg)
        for execution in self.executions_collection.find(filter, { "_id":0 }).limit(50).sort("starttime", DESCENDING):
            self.__enrich_with_inner_results(execution, requestorName, requestorOrg)
            out.append(execution)
        return out

    def add_execution(self, execution, requestorName=None):
        self.executions_collection.insert_one(execution)
        return self.get_execution(execution['id'])

    def update_execution(self, execution, requestorName=None):
        self.executions_collection.replace_one({"id":execution['id']}, execution)
        return self.get_execution(execution['id'], requestorName)

    def get_execution(self, execution_id, requestorName=None, requestorOrg=None):
#        p = self.executions_collection.find_one({"_id": ObjectId(execution_id)})
        filter = self._getExecutionFilter(execution_id, requestorName=requestorName, requestorOrg=requestorOrg)
        e = self.executions_collection.find_one(filter, {"_id":0})
        if e:
            self.__enrich_with_inner_results(e, requestorName, requestorOrg)
        return e

    def list_runs(self, execution_id, requestorName=None, requestorOrg=None, hide=False):
        out = []
        
        runfields = None
        errorfields = None
        if hide:
            execution = self.get_execution(execution_id, requestorName, requestorOrg)
            if not execution:
                return out
            runfields = self._get_run_fields(execution, requestorName)
            errorfields = self._get_run_error_fields(execution, requestorName)

        # retrieve available reports/errors
        out.extend(self.results_collection.find({"properties.execution_id":execution_id}, runfields))
        out.extend(self.errors_collection.find({"properties.execution_id": execution_id}, errorfields))

        return out
        
    @staticmethod
    def load_connector():
        o = MongoDBExecutionConnector()
        o.client = MongoClient("mongodb://"+os.environ['DB_HOST'])
        o.db = o.client[os.environ["DB_NAME"]]
#        o.executions_collection = o.db["_apexec"]
        o.executions_collection = o.db["executions"]
        o.results_collection = o.db["results"]
        o.errors_collection = o.db["exec_errors"]
        o.executions_collection.create_index([('time', DESCENDING)], unique=False)
        return o
