#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from pymongo import MongoClient

from benchsuite.core.util.crypto import AESCipher


class MongoConnectorBase(object):

    def __init__(self, db_host='localhost', db_port=27017, db_name='benchsuite', db_username=None, db_password=None, db_aes_key=None, **kwargs):
        self.client = MongoClient(db_host, int(db_port), username=db_username, password=db_password)
        self.db = self.client[db_name]
        self.crypto = AESCipher(db_aes_key=db_aes_key, **kwargs)
